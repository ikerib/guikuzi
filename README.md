GUIKUZI
========================

Control por código de barras de los huecos/productos y el mantenimiento y confiración de las mismas.

OHARRAK!!!
----------------------------------

### app/config

  * parameters => Centro_por_defecto zehaztu


### DELETE akzioarako

#### Entitian
```
      public function getCsrfIntention($intention) {
        return sha1(get_class($this).$intention.$this->id);
      }
```
#### Twig
```
<a href="{{ path('entity_delete', { 'id' : entity.id, 'token': csrf_token(entity.csrfIntention('delete')) })}}" onclick="return confirm('Are you sure?');">Delete</a>
```

#### Controller
```
public function deleteAction($id, $token) {
	$entity = /* .. get entity .. */
	$csrf  = $this->container->get('form.csrf_provider');

   	if ($csrf->isCsrfTokenValid($entity->getCsrfIntention('delete'), $token))
   	{
	    //delete the entity
    } else {
	    //throw error - token is incorrect
	}
}
```
### REST
  * http://blog.logicexception.com/2012/04/setting-up-symfony2-rest-service-with.html
