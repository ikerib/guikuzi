-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.6.11-log - MySQL Community Server (GPL)
-- SO del servidor:              Win32
-- HeidiSQL Versión:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura para tabla guikuzi.centro
DROP TABLE IF EXISTS `centro`;
CREATE TABLE IF NOT EXISTS `centro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla guikuzi.centro: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `centro` DISABLE KEYS */;
/*!40000 ALTER TABLE `centro` ENABLE KEYS */;


-- Volcando estructura para tabla guikuzi.config
DROP TABLE IF EXISTS `config`;
CREATE TABLE IF NOT EXISTS `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `habitacion_id` int(11) DEFAULT NULL,
  `jaula_id` int(11) DEFAULT NULL,
  `jauladet_id` int(11) DEFAULT NULL,
  `residente_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D3262A4AB009290D` (`habitacion_id`),
  KEY `IDX_D3262A4A147FAE31` (`jaula_id`),
  KEY `IDX_D3262A4A4882FAB9` (`jauladet_id`),
  KEY `IDX_D3262A4A59E2142F` (`residente_id`),
  CONSTRAINT `FK_D3262A4A147FAE31` FOREIGN KEY (`jaula_id`) REFERENCES `jaula` (`id`),
  CONSTRAINT `FK_D3262A4A4882FAB9` FOREIGN KEY (`jauladet_id`) REFERENCES `jauladet` (`id`),
  CONSTRAINT `FK_D3262A4A59E2142F` FOREIGN KEY (`residente_id`) REFERENCES `residente` (`id`),
  CONSTRAINT `FK_D3262A4AB009290D` FOREIGN KEY (`habitacion_id`) REFERENCES `habitacion` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla guikuzi.config: ~20 rows (aproximadamente)
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` (`id`, `habitacion_id`, `jaula_id`, `jauladet_id`, `residente_id`, `created_at`, `updated_at`, `nombre`) VALUES
	(1, 4, 5, 1, 1, '2014-04-01 10:45:57', '2014-04-01 10:45:57', NULL),
	(2, 5, 5, 2, 2, '2014-04-01 10:46:17', '2014-04-01 10:46:17', NULL),
	(3, 6, 5, 3, 5, '2014-04-01 10:46:36', '2014-04-01 10:46:36', NULL),
	(4, 7, 5, 4, 80, '2014-04-01 10:48:14', '2014-04-01 10:48:14', NULL),
	(5, 8, 5, 5, 6, '2014-04-01 10:48:30', '2014-04-01 10:48:30', NULL),
	(6, 9, 5, 6, 7, '2014-04-01 10:48:45', '2014-04-01 10:48:45', NULL),
	(7, 10, 5, 7, 8, '2014-04-01 10:48:59', '2014-04-01 10:48:59', NULL),
	(8, 11, 5, 8, 9, '2014-04-01 10:49:16', '2014-04-01 10:49:16', NULL),
	(9, 12, 5, 9, 10, '2014-04-01 10:49:35', '2014-04-01 10:49:35', NULL),
	(10, 13, 5, 10, NULL, '2014-04-01 10:49:53', '2014-04-01 10:49:53', NULL),
	(11, 14, 5, 11, 12, '2014-04-01 10:50:16', '2014-04-01 10:50:16', NULL),
	(12, 15, 5, 12, NULL, '2014-04-01 10:50:37', '2014-04-01 10:50:37', NULL),
	(13, 16, 5, 13, 3, '2014-04-01 10:51:06', '2014-04-01 10:51:06', NULL),
	(14, 17, 5, 14, 4, '2014-04-01 10:51:24', '2014-04-01 10:51:24', NULL),
	(15, 18, 5, 15, NULL, '2014-04-01 10:51:41', '2014-04-01 10:51:41', NULL),
	(16, 19, 5, 16, 55, '2014-04-01 10:52:03', '2014-04-01 10:52:03', NULL),
	(17, 20, 5, 17, 16, '2014-04-01 10:52:22', '2014-04-01 10:52:22', NULL),
	(18, 21, 5, 18, 17, '2014-04-01 10:52:39', '2014-04-01 10:52:39', NULL),
	(19, 22, 5, 19, 18, '2014-04-01 10:53:00', '2014-04-01 10:53:00', NULL),
	(20, 23, 5, 20, NULL, '2014-04-01 10:53:19', '2014-04-01 10:53:19', NULL);
/*!40000 ALTER TABLE `config` ENABLE KEYS */;


-- Volcando estructura para tabla guikuzi.habitacion
DROP TABLE IF EXISTS `habitacion`;
CREATE TABLE IF NOT EXISTS `habitacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `codigo` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla guikuzi.habitacion: ~102 rows (aproximadamente)
/*!40000 ALTER TABLE `habitacion` DISABLE KEYS */;
INSERT INTO `habitacion` (`id`, `nombre`, `codigo`, `created_at`, `updated_at`) VALUES
	(4, 'Habitación 501A', 'H501A', '2013-03-26 14:35:39', '2013-03-26 14:35:39'),
	(5, 'Habitación 501B', 'H501B', '2013-03-26 14:36:08', '2013-03-26 14:36:08'),
	(6, 'Habitación 502A', 'H502A', '2013-03-27 10:37:22', '2013-03-27 10:37:22'),
	(7, 'Habitación 502B', 'H502B', '2013-03-27 10:37:44', '2013-03-27 10:37:44'),
	(8, 'Habitación 503A', 'H503A', '2013-03-27 10:37:58', '2013-03-27 10:37:58'),
	(9, 'Habitación 503B', 'H503B', '2013-03-27 10:38:13', '2013-03-27 10:38:13'),
	(10, 'Habitación 504A', 'H504A', '2013-03-27 10:49:40', '2013-03-27 10:49:40'),
	(11, 'Habitación 504B', 'H504B', '2013-04-16 16:31:38', '2013-04-16 16:31:38'),
	(12, 'Habitación 505A', 'H505A', '2013-04-16 16:31:52', '2013-04-16 16:31:52'),
	(13, 'Habitación 505B', 'H505B', '2013-04-16 16:32:05', '2013-04-16 16:32:05'),
	(14, 'Habitación 506A', 'H506A', '2013-04-16 16:32:32', '2013-04-16 16:32:32'),
	(15, 'Habitación 506B', 'H506B', '2013-04-16 16:33:22', '2013-04-16 16:33:22'),
	(16, 'Habitación 507A', 'H507A', '2013-04-16 16:35:08', '2013-04-16 16:35:08'),
	(17, 'Habitación 507B', 'H507B', '2013-04-16 16:35:35', '2013-04-16 16:35:35'),
	(18, 'Habitación 509A', 'H509A', '2013-04-16 16:35:47', '2013-04-16 16:35:47'),
	(19, 'Habitación 509B', 'H509B', '2013-04-16 16:35:59', '2013-04-16 16:35:59'),
	(20, 'Habitación 510A', 'H510A', '2013-04-16 16:36:11', '2013-04-16 16:36:11'),
	(21, 'Habitación 510B', 'H510B', '2013-04-16 16:36:20', '2013-04-16 16:36:20'),
	(22, 'Habitación 511A', 'H511A', '2013-04-16 16:36:35', '2013-04-16 16:36:35'),
	(23, 'Habitación 511B', 'H511B', '2013-04-16 16:36:45', '2013-04-16 16:36:45'),
	(24, 'Habitación 512', 'H512', '2013-04-16 16:37:06', '2013-04-16 16:37:06'),
	(25, 'Habitación 601A', 'H601A', '2013-04-16 16:37:25', '2013-04-16 16:37:25'),
	(26, 'Habitación 601B', 'H601B', '2013-04-16 16:37:37', '2013-04-16 16:37:37'),
	(27, 'Habitación 602A', 'H602A', '2013-04-16 16:37:50', '2013-04-16 16:37:50'),
	(28, 'Habitación 602B', 'H602B', '2013-04-16 16:38:01', '2013-04-16 16:38:01'),
	(29, 'Habitación 603A', 'H603A', '2013-04-16 16:38:19', '2013-04-16 16:38:19'),
	(30, 'Habitación 603B', 'H603B', '2013-04-16 16:38:31', '2013-04-16 16:38:31'),
	(31, 'Habitación 604A', 'H604A', '2013-04-16 16:38:42', '2013-04-16 16:38:42'),
	(32, 'Habitación 604B', 'H604B', '2013-04-16 16:38:56', '2013-04-16 16:38:56'),
	(33, 'Habitación 605A', 'H605A', '2013-04-16 16:39:11', '2013-04-16 16:39:11'),
	(34, 'Habitación 605B', 'H605B', '2013-04-16 16:39:20', '2013-04-16 16:39:20'),
	(35, 'Habitación 606A', 'H606A', '2013-04-16 16:39:32', '2013-04-16 16:39:32'),
	(36, 'Habitación 606B', 'H606B', '2013-04-16 16:39:48', '2013-04-16 16:39:48'),
	(37, 'Habitación 607A', 'H607A', '2013-04-16 16:40:02', '2013-04-16 16:40:02'),
	(38, 'Habitación 607B', 'H607B', '2013-04-16 16:40:18', '2013-04-16 16:40:18'),
	(39, 'Habitación 608A', 'H608A', '2013-04-16 16:40:30', '2013-04-16 16:40:30'),
	(40, 'Habitación 608B', 'H608B', '2013-04-16 16:40:39', '2013-04-16 16:40:39'),
	(41, 'Habitación 609', 'H609', '2013-04-16 16:40:50', '2013-04-16 16:40:50'),
	(42, 'Habitación 610', 'H610', '2013-04-16 16:40:57', '2013-04-16 16:40:57'),
	(43, 'Habitación 611', 'H611', '2013-04-16 16:41:08', '2013-04-16 16:41:08'),
	(44, 'Habitación 612A', 'H612A', '2013-04-16 16:41:27', '2013-04-16 16:41:27'),
	(45, 'Habitación 612B', 'H612B', '2013-04-16 16:41:36', '2013-04-16 16:41:36'),
	(46, 'Habitación 701A', 'H701A', '2013-04-16 16:41:51', '2013-04-16 16:41:51'),
	(47, 'Habitación 701B', 'H701B', '2013-04-16 16:42:00', '2013-04-16 16:42:00'),
	(48, 'Habitación 702A', 'H702A', '2013-04-16 16:42:21', '2013-04-16 16:42:21'),
	(49, 'Habitación 702B', 'H702B', '2013-04-16 16:42:30', '2013-04-16 16:42:30'),
	(50, 'Habitación 703A', 'H703A', '2013-04-16 16:42:40', '2013-04-16 16:42:40'),
	(51, 'Habitación 703B', 'H703B', '2013-04-16 16:42:50', '2013-04-16 16:42:50'),
	(52, 'Habitación 704A', 'H704A', '2013-04-16 16:43:00', '2013-04-16 16:43:00'),
	(53, 'Habitación 704B', 'H704B', '2013-04-16 16:43:16', '2013-04-16 16:43:16'),
	(54, 'Habitación 705A', 'H705A', '2013-04-16 16:43:29', '2013-04-16 16:43:29'),
	(55, 'Habitación 705B', 'H705B', '2013-04-16 16:43:38', '2013-04-16 16:43:38'),
	(56, 'Habitación 706A', 'H706A', '2013-04-16 16:43:52', '2013-04-16 16:43:52'),
	(57, 'Habitación 706B', 'H706B', '2013-04-16 16:44:02', '2013-04-16 16:44:02'),
	(58, 'Habitación 707A', 'H707A', '2013-04-16 16:44:13', '2013-04-16 16:44:13'),
	(59, 'Habitación 707B', 'H707B', '2013-04-16 16:44:21', '2013-04-16 16:44:21'),
	(60, 'Habitación 708A', 'H708A', '2013-04-16 16:44:40', '2013-04-16 16:44:40'),
	(61, 'Habitación 708B', 'H708B', '2013-04-16 16:44:52', '2013-04-16 16:44:52'),
	(62, 'Habitación 714', 'H714', '2013-04-16 16:45:11', '2013-04-16 16:45:11'),
	(63, 'Habitación 715', 'H715', '2013-04-16 16:45:20', '2013-04-16 16:45:20'),
	(64, 'Habitación 716', 'H716', '2013-04-16 16:45:31', '2013-04-16 16:45:31'),
	(65, 'Habitación 717A', 'H717A', '2013-04-16 16:45:47', '2013-04-16 16:45:47'),
	(66, 'Habitación 717B', 'H717B', '2013-04-16 16:45:58', '2013-04-16 16:45:58'),
	(67, 'Habitación 801A', 'H801A', '2013-04-16 16:46:11', '2013-04-16 16:46:11'),
	(68, 'Habitación 801B', 'H801B', '2013-04-16 16:46:23', '2013-04-16 16:46:23'),
	(69, 'Habitación 802A', 'H802A', '2013-04-16 16:46:33', '2013-04-16 16:46:33'),
	(70, 'Habitación 802B', 'H802B', '2013-04-16 16:46:45', '2013-04-16 16:46:45'),
	(71, 'Habitación 803A', 'H803A', '2013-04-16 16:46:55', '2013-04-16 16:46:55'),
	(72, 'Habitación 803B', 'H803B', '2013-04-16 16:47:04', '2013-04-16 16:47:04'),
	(73, 'Habitación 804A', 'H804A', '2013-04-16 16:47:15', '2013-04-16 16:47:15'),
	(74, 'Habitación 804B', 'H804B', '2013-04-16 16:47:24', '2013-04-16 16:47:24'),
	(75, 'Habitación 805A', 'H805A', '2013-04-16 16:47:34', '2013-04-16 16:47:34'),
	(76, 'Habitación 805B', 'H805B', '2013-04-16 16:47:43', '2013-04-16 16:47:43'),
	(77, 'Habitación 806A', 'H806A', '2013-04-16 16:47:54', '2013-04-16 16:47:54'),
	(78, 'Habitación 806B', 'H806B', '2013-04-16 16:48:07', '2013-04-16 16:48:07'),
	(79, 'Habitación 807A', 'H807A', '2013-04-16 16:48:18', '2013-04-16 16:48:18'),
	(80, 'Habitación 807B', 'H807B', '2013-04-16 16:48:26', '2013-04-16 16:48:26'),
	(81, 'Habitación 813', 'H813', '2013-04-16 16:48:38', '2013-04-16 16:48:38'),
	(82, 'Habitación 814', 'H814', '2013-04-16 16:48:48', '2013-04-16 16:48:48'),
	(83, 'Habitación 815', 'H815', '2013-04-16 16:48:57', '2013-04-16 16:48:57'),
	(84, 'Habitación 816A', 'H816A', '2013-04-16 16:49:07', '2013-04-16 16:49:07'),
	(85, 'Habitación 816B', 'H816B', '2013-04-16 16:49:15', '2013-04-16 16:49:15'),
	(86, 'Habitación 901A', 'H901A', '2013-04-16 16:49:36', '2013-04-16 16:49:36'),
	(87, 'Habitación 901B', 'H901B', '2013-04-16 16:49:45', '2013-04-16 16:49:45'),
	(88, 'Habitación 902A', 'H902A', '2013-04-16 16:49:55', '2013-04-16 16:49:55'),
	(89, 'Habitación 902B', 'H902B', '2013-04-16 16:50:03', '2013-04-16 16:50:03'),
	(90, 'Habitación 903A', 'H903A', '2013-04-16 16:50:18', '2013-04-16 16:50:18'),
	(91, 'Habitación 903B', 'H903B', '2013-04-16 16:50:27', '2013-04-16 16:50:27'),
	(92, 'Habitación 904A', 'H904A', '2013-04-16 16:50:36', '2013-04-16 16:50:36'),
	(93, 'Habitación 904B', 'H904B', '2013-04-16 16:50:45', '2013-04-16 16:50:45'),
	(94, 'Habitación 905A', 'H905A', '2013-04-16 16:50:55', '2013-04-16 16:50:55'),
	(95, 'Habitación 905B', 'H905B', '2013-04-16 16:51:04', '2013-04-16 16:51:04'),
	(96, 'Habitación 906A', 'H906A', '2013-04-16 16:51:14', '2013-04-16 16:51:14'),
	(97, 'Habitación 906B', 'H906B', '2013-04-16 16:51:25', '2013-04-16 16:51:25'),
	(98, 'Habitación 907A', 'H907A', '2013-04-16 16:51:34', '2013-04-16 16:51:34'),
	(99, 'Habitación 907B', 'H907B', '2013-04-16 16:51:44', '2013-04-16 16:51:44'),
	(100, 'Habitación 908', 'H908', '2013-04-16 16:51:53', '2013-04-16 16:51:53'),
	(101, 'Habitación 909', 'H909', '2013-04-16 16:52:01', '2013-04-16 16:52:01'),
	(102, 'Habitación 910A', 'H910A', '2013-04-16 16:52:11', '2013-04-16 16:52:11'),
	(103, 'Habitación 910B', 'H910B', '2013-04-16 16:52:20', '2013-04-16 16:52:20'),
	(104, 'Habitación 911A', 'H911A', '2013-04-16 16:52:31', '2013-04-16 16:52:31'),
	(105, 'Habitación 911B', 'H911B', '2013-04-16 16:52:41', '2013-04-16 16:52:41');
/*!40000 ALTER TABLE `habitacion` ENABLE KEYS */;


-- Volcando estructura para tabla guikuzi.jaula
DROP TABLE IF EXISTS `jaula`;
CREATE TABLE IF NOT EXISTS `jaula` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `comprobar` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla guikuzi.jaula: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `jaula` DISABLE KEYS */;
INSERT INTO `jaula` (`id`, `nombre`, `created_at`, `updated_at`, `comprobar`) VALUES
	(5, 'Jaula 1', '2014-04-01 10:18:44', '2014-04-01 10:18:44', 0);
/*!40000 ALTER TABLE `jaula` ENABLE KEYS */;


-- Volcando estructura para tabla guikuzi.jauladet
DROP TABLE IF EXISTS `jauladet`;
CREATE TABLE IF NOT EXISTS `jauladet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jaula_id` int(11) DEFAULT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_7435997E147FAE31` (`jaula_id`),
  CONSTRAINT `FK_7435997E147FAE31` FOREIGN KEY (`jaula_id`) REFERENCES `jaula` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla guikuzi.jauladet: ~36 rows (aproximadamente)
/*!40000 ALTER TABLE `jauladet` DISABLE KEYS */;
INSERT INTO `jauladet` (`id`, `jaula_id`, `foto`, `created_at`, `updated_at`, `nombre`) VALUES
	(1, 5, '533a7abb577fe.jpeg', '2014-04-01 10:37:15', '2014-04-01 11:03:09', '501A'),
	(2, 5, '533a7acfe0d42.jpeg', '2014-04-01 10:37:35', '2014-04-01 11:03:09', '501B'),
	(3, 5, '533a7adac4b20.jpeg', '2014-04-01 10:37:46', '2014-04-01 11:03:09', '502A'),
	(4, 5, '533a7ae34d025.jpeg', '2014-04-01 10:37:55', '2014-04-01 11:03:09', '502B'),
	(5, 5, '533a7aee0b34a.jpeg', '2014-04-01 10:38:05', '2014-04-01 11:03:09', '503A'),
	(6, 5, '533a7afb44aa2.jpeg', '2014-04-01 10:38:19', '2014-04-01 11:03:09', '503B'),
	(7, 5, '533a7b07907b4.jpeg', '2014-04-01 10:38:31', '2014-04-01 11:03:09', '504A'),
	(8, 5, '533a7b12405f7.jpeg', '2014-04-01 10:38:42', '2014-04-01 11:03:09', '504B'),
	(9, 5, '533a7b21ab2f3.jpeg', '2014-04-01 10:38:57', '2014-04-01 11:03:09', '505A'),
	(10, 5, '533a7b2dd5627.jpeg', '2014-04-01 10:39:09', '2014-04-01 11:03:09', '505B'),
	(11, 5, '533a7b36645fc.jpeg', '2014-04-01 10:39:18', '2014-04-01 11:03:09', '506A'),
	(12, 5, '533a7b3ecb5ef.jpeg', '2014-04-01 10:39:26', '2014-04-01 11:03:09', '506B'),
	(13, 5, '533a7b522c959.jpeg', '2014-04-01 10:39:45', '2014-04-01 11:03:09', '507A'),
	(14, 5, '533a7b5d9e8c6.jpeg', '2014-04-01 10:39:57', '2014-04-01 11:03:09', '507B'),
	(15, 5, '533a7b6dc5a62.jpeg', '2014-04-01 10:40:13', '2014-04-01 11:03:09', '509A'),
	(16, 5, '533a7b79094c6.jpeg', '2014-04-01 10:40:24', '2014-04-01 11:03:09', '509B'),
	(17, 5, '533a7bae96ae3.jpeg', '2014-04-01 10:41:18', '2014-04-01 11:03:09', '510A'),
	(18, 5, '533a7bbf6b86d.jpeg', '2014-04-01 10:41:35', '2014-04-01 11:03:09', '510B'),
	(19, 5, '533a7bcba074a.jpeg', '2014-04-01 10:41:47', '2014-04-01 11:03:09', '511A'),
	(20, 5, '533a7bd7b9ba6.jpeg', '2014-04-01 10:41:59', '2014-04-01 11:03:09', '511B'),
	(21, 5, '533a7be613c9e.jpeg', '2014-04-01 10:42:13', '2014-04-01 11:03:09', '512A'),
	(22, 5, '533a7bf7b6a0e.jpeg', '2014-04-01 10:42:31', '2014-04-01 11:03:09', '512B'),
	(23, 5, '533a7c30f1feb.jpeg', '2014-04-01 10:43:28', '2014-04-01 11:03:09', '520B'),
	(24, 5, '533a7c3b04c4b.jpeg', '2014-04-01 10:43:38', '2014-04-01 11:03:09', '521'),
	(25, 5, '533a7c465fd82.jpeg', '2014-04-01 10:43:50', '2014-04-01 11:03:09', '522'),
	(26, 5, '533a7c5022921.jpeg', '2014-04-01 10:43:59', '2014-04-01 11:03:09', '523'),
	(27, 5, '533a7c5937132.jpeg', '2014-04-01 10:44:09', '2014-04-01 11:03:09', '524'),
	(28, 5, '533a7c61e9e37.jpeg', '2014-04-01 10:44:17', '2014-04-01 11:03:09', '525'),
	(29, 5, '533a7c6a0a7d8.jpeg', '2014-04-01 10:44:25', '2014-04-01 11:03:09', '526'),
	(30, 5, '533a7c740cdfe.jpeg', '2014-04-01 10:44:35', '2014-04-01 11:03:09', '527'),
	(31, 5, '533a7c7b92a0a.jpeg', '2014-04-01 10:44:43', '2014-04-01 11:03:09', '528'),
	(32, 5, '533a7c84256e8.jpeg', '2014-04-01 10:44:51', '2014-04-01 11:03:09', '529'),
	(33, 5, '533a7c8e60152.jpeg', '2014-04-01 10:45:02', '2014-04-01 11:03:09', '530'),
	(34, 5, '533a7c9b30291.jpeg', '2014-04-01 10:45:14', '2014-04-01 11:03:09', '531'),
	(35, 5, '533a7ca414810.jpeg', '2014-04-01 10:45:23', '2014-04-01 11:03:09', '532'),
	(36, 5, '533a7cacb4b8a.jpeg', '2014-04-01 10:45:32', '2014-04-01 11:03:09', '533');
/*!40000 ALTER TABLE `jauladet` ENABLE KEYS */;


-- Volcando estructura para tabla guikuzi.parametros
DROP TABLE IF EXISTS `parametros`;
CREATE TABLE IF NOT EXISTS `parametros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `centro_id` int(11) DEFAULT NULL,
  `comprobar` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_AFCB92C7298137A7` (`centro_id`),
  CONSTRAINT `FK_AFCB92C7298137A7` FOREIGN KEY (`centro_id`) REFERENCES `centro` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla guikuzi.parametros: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `parametros` DISABLE KEYS */;
/*!40000 ALTER TABLE `parametros` ENABLE KEYS */;


-- Volcando estructura para tabla guikuzi.registro
DROP TABLE IF EXISTS `registro`;
CREATE TABLE IF NOT EXISTS `registro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `resultado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla guikuzi.registro: ~132 rows (aproximadamente)
/*!40000 ALTER TABLE `registro` DISABLE KEYS */;
INSERT INTO `registro` (`id`, `resultado`, `created_at`, `updated_at`) VALUES
	(1, 'Jaula: jaula5hab501A // Foto: 5190ec54126c5.jpeg', '2013-05-24 10:10:51', '2013-05-24 10:10:51'),
	(2, 'Error', '2013-05-24 10:11:05', '2013-05-24 10:11:05'),
	(3, 'Jaula: jaula5hab501A // Foto: 5190ec54126c5.jpeg', '2013-05-24 10:11:12', '2013-05-24 10:11:12'),
	(4, 'Error', '2013-05-29 16:06:08', '2013-05-29 16:06:08'),
	(5, 'Error', '2013-05-29 16:06:09', '2013-05-29 16:06:09'),
	(6, 'Error', '2013-05-29 16:06:11', '2013-05-29 16:06:11'),
	(7, 'Jaula: jaula5hab501A // Foto: 519f23902082e.jpeg', '2013-06-03 12:33:42', '2013-06-03 12:33:42'),
	(8, 'Error', '2013-06-03 12:34:09', '2013-06-03 12:34:09'),
	(9, 'Error', '2013-06-03 12:34:23', '2013-06-03 12:34:23'),
	(10, 'Error', '2013-06-03 12:35:36', '2013-06-03 12:35:36'),
	(11, 'Error', '2013-06-03 12:35:40', '2013-06-03 12:35:40'),
	(12, 'Error', '2013-06-03 12:35:43', '2013-06-03 12:35:43'),
	(13, 'Error', '2013-06-03 12:35:50', '2013-06-03 12:35:50'),
	(14, 'Error', '2013-06-03 12:36:03', '2013-06-03 12:36:03'),
	(15, 'Jaula: jaula5hab505B // Foto: 519f23dea033b.jpeg', '2013-06-03 12:36:25', '2013-06-03 12:36:25'),
	(16, 'Error', '2013-06-03 12:36:31', '2013-06-03 12:36:31'),
	(17, 'Jaula: jaula5hab505B // Foto: 519f23dea033b.jpeg', '2013-06-03 12:36:33', '2013-06-03 12:36:33'),
	(18, 'Jaula: jaula5hab505B // Foto: 519f23dea033b.jpeg', '2013-06-03 12:36:44', '2013-06-03 12:36:44'),
	(19, 'Jaula: jaula5hab501A // Foto: 519f23902082e.jpeg', '2013-06-03 12:36:58', '2013-06-03 12:36:58'),
	(20, 'Jaula: jaula5hab501A // Foto: 519f23902082e.jpeg', '2013-06-03 12:39:36', '2013-06-03 12:39:36'),
	(21, 'Jaula: jaula5hab501A // Foto: 519f23902082e.jpeg', '2013-06-03 12:40:14', '2013-06-03 12:40:14'),
	(22, 'Jaula: jaula5hab501A // Foto: 519f23902082e.jpeg', '2013-06-03 12:40:45', '2013-06-03 12:40:45'),
	(23, 'Error', '2013-06-03 12:41:07', '2013-06-03 12:41:07'),
	(24, 'Error', '2013-06-10 10:44:04', '2013-06-10 10:44:04'),
	(25, 'Jaula: jaula5hab505B // Foto: 519f23dea033b.jpeg', '2013-06-10 10:44:25', '2013-06-10 10:44:25'),
	(26, 'Jaula: jaula5hab501A // Foto: 519f23902082e.jpeg', '2013-06-10 13:15:50', '2013-06-10 13:15:50'),
	(27, 'Jaula: jaula5hab501A // Foto: 519f23902082e.jpeg', '2013-06-10 13:15:54', '2013-06-10 13:15:54'),
	(28, 'Error', '2013-06-10 13:16:17', '2013-06-10 13:16:17'),
	(29, 'Jaula: jaula5hab505B // Foto: 519f23dea033b.jpeg', '2013-06-10 13:16:58', '2013-06-10 13:16:58'),
	(30, 'Error', '2013-06-10 13:48:16', '2013-06-10 13:48:16'),
	(31, 'Error', '2013-06-10 13:48:19', '2013-06-10 13:48:19'),
	(32, 'Comprobar OK', '2013-06-10 13:48:28', '2013-06-10 13:48:28'),
	(33, 'Jaula: jaula5hab505B // Foto: 519f23dea033b.jpeg', '2013-06-10 15:10:00', '2013-06-10 15:10:00'),
	(34, 'Error', '2013-06-10 15:10:02', '2013-06-10 15:10:02'),
	(35, 'Error', '2013-06-10 15:10:05', '2013-06-10 15:10:05'),
	(36, 'Error', '2013-06-10 15:10:07', '2013-06-10 15:10:07'),
	(37, 'Error', '2013-06-10 15:10:11', '2013-06-10 15:10:11'),
	(38, 'Comprobar OK', '2013-06-10 15:10:13', '2013-06-10 15:10:13'),
	(39, 'Error', '2013-06-10 15:20:20', '2013-06-10 15:20:20'),
	(40, 'Error', '2013-06-10 15:20:22', '2013-06-10 15:20:22'),
	(41, 'Error', '2013-06-10 15:20:25', '2013-06-10 15:20:25'),
	(42, 'Error', '2013-06-10 15:20:27', '2013-06-10 15:20:27'),
	(43, 'Jaula: jaula5hab505B // Foto: 519f23dea033b.jpeg', '2013-06-12 10:40:08', '2013-06-12 10:40:08'),
	(44, 'Comprobar OK', '2013-06-12 10:40:17', '2013-06-12 10:40:17'),
	(45, 'Jaula: jaula5hab505B // Foto: 519f23dea033b.jpeg', '2013-06-12 10:41:01', '2013-06-12 10:41:01'),
	(46, 'Comprobar OK', '2013-06-12 10:41:07', '2013-06-12 10:41:07'),
	(47, 'Jaula: jaula5hab505B // Foto: 519f23dea033b.jpeg', '2013-06-12 10:46:31', '2013-06-12 10:46:31'),
	(48, 'Comprobar OK', '2013-06-12 10:46:37', '2013-06-12 10:46:37'),
	(49, 'Jaula: jaula5hab505B // Foto: 519f23dea033b.jpeg', '2013-06-12 10:57:46', '2013-06-12 10:57:46'),
	(50, 'Comprobar OK', '2013-06-12 10:58:43', '2013-06-12 10:58:43'),
	(51, 'Jaula: jaula5hab501A // Foto: 519f23902082e.jpeg', '2013-06-13 16:13:34', '2013-06-13 16:13:34'),
	(52, 'Error', '2013-06-13 16:13:42', '2013-06-13 16:13:42'),
	(53, 'Comprobar OK', '2013-06-13 16:13:47', '2013-06-13 16:13:47'),
	(54, 'Jaula: jaula5hab501A // Foto: 519f23902082e.jpeg', '2013-06-13 16:15:06', '2013-06-13 16:15:06'),
	(55, 'Error', '2013-06-13 16:15:15', '2013-06-13 16:15:15'),
	(56, 'Comprobar OK', '2013-06-13 16:15:21', '2013-06-13 16:15:21'),
	(57, 'Jaula: jaula5hab501A // Foto: 519f23902082e.jpeg', '2013-06-13 16:16:24', '2013-06-13 16:16:24'),
	(58, 'Error', '2013-06-13 16:16:39', '2013-06-13 16:16:39'),
	(59, 'Error', '2013-06-13 16:16:57', '2013-06-13 16:16:57'),
	(60, 'Comprobar OK', '2013-06-13 16:17:00', '2013-06-13 16:17:00'),
	(61, 'Error', '2013-06-13 16:17:14', '2013-06-13 16:17:14'),
	(62, 'Jaula: jaula5hab505B // Foto: 519f23dea033b.jpeg', '2013-06-13 16:17:17', '2013-06-13 16:17:17'),
	(63, 'Comprobar OK', '2013-06-13 16:17:28', '2013-06-13 16:17:28'),
	(64, 'Error', '2013-06-13 16:17:48', '2013-06-13 16:17:48'),
	(65, 'Jaula: jaula5hab505B // Foto: 519f23dea033b.jpeg', '2013-06-13 16:17:51', '2013-06-13 16:17:51'),
	(66, 'Error', '2013-06-13 16:17:56', '2013-06-13 16:17:56'),
	(67, 'Comprobar OK', '2013-06-13 16:17:58', '2013-06-13 16:17:58'),
	(68, 'Jaula: jaula5hab505B // Foto: 519f23dea033b.jpeg', '2013-06-13 16:18:02', '2013-06-13 16:18:02'),
	(69, 'Error', '2013-06-13 16:18:05', '2013-06-13 16:18:05'),
	(70, 'Comprobar OK', '2013-06-13 16:18:08', '2013-06-13 16:18:08'),
	(71, 'Jaula: jaula5hab505B // Foto: 519f23dea033b.jpeg', '2013-06-13 16:18:11', '2013-06-13 16:18:11'),
	(72, 'Error', '2013-06-13 16:18:16', '2013-06-13 16:18:16'),
	(73, 'Error', '2013-06-13 16:18:18', '2013-06-13 16:18:18'),
	(74, 'Comprobar OK', '2013-06-13 16:18:20', '2013-06-13 16:18:20'),
	(75, 'Jaula: jaula5hab505B // Foto: 519f23dea033b.jpeg', '2013-06-13 16:18:22', '2013-06-13 16:18:22'),
	(76, 'Comprobar OK', '2013-06-13 16:18:26', '2013-06-13 16:18:26'),
	(77, 'Jaula: jaula5hab505B // Foto: 519f23dea033b.jpeg', '2013-06-13 16:18:27', '2013-06-13 16:18:27'),
	(78, 'Comprobar OK', '2013-06-13 16:18:31', '2013-06-13 16:18:31'),
	(79, 'Jaula: jaula5hab501A // Foto: 519f23902082e.jpeg', '2013-06-13 16:18:35', '2013-06-13 16:18:35'),
	(80, 'Error', '2013-06-13 16:18:38', '2013-06-13 16:18:38'),
	(81, 'Comprobar OK', '2013-06-13 16:18:39', '2013-06-13 16:18:39'),
	(82, 'Jaula: jaula5hab505B // Foto: 519f23dea033b.jpeg', '2013-06-14 09:25:02', '2013-06-14 09:25:02'),
	(83, 'Comprobar OK', '2013-06-14 09:25:11', '2013-06-14 09:25:11'),
	(84, 'Error', '2013-06-14 09:25:20', '2013-06-14 09:25:20'),
	(85, 'Jaula: jaula5hab505B // Foto: 519f23dea033b.jpeg', '2013-06-14 09:25:24', '2013-06-14 09:25:24'),
	(86, 'Error', '2013-06-14 09:25:31', '2013-06-14 09:25:31'),
	(87, 'Error', '2013-06-14 09:25:35', '2013-06-14 09:25:35'),
	(88, 'Error', '2013-06-14 09:25:47', '2013-06-14 09:25:47'),
	(89, 'Jaula: jaula5hab505B // Foto: 519f23dea033b.jpeg', '2013-06-14 11:45:52', '2013-06-14 11:45:52'),
	(90, 'Error', '2013-06-14 11:45:59', '2013-06-14 11:45:59'),
	(91, 'Comprobar OK', '2013-06-14 11:46:01', '2013-06-14 11:46:01'),
	(92, 'Jaula: jaula5hab505B // Foto: 519f23dea033b.jpeg', '2013-06-14 11:48:41', '2013-06-14 11:48:41'),
	(93, 'Error', '2013-06-14 11:48:49', '2013-06-14 11:48:49'),
	(94, 'Error', '2013-06-14 11:48:51', '2013-06-14 11:48:51'),
	(95, 'Error', '2013-06-14 11:48:57', '2013-06-14 11:48:57'),
	(96, 'Error', '2013-06-14 11:49:05', '2013-06-14 11:49:05'),
	(97, 'Error', '2013-06-14 11:49:07', '2013-06-14 11:49:07'),
	(98, 'Comprobar OK', '2013-06-14 11:49:09', '2013-06-14 11:49:09'),
	(99, 'Error', '2013-07-01 13:00:40', '2013-07-01 13:00:40'),
	(100, 'Error', '2013-07-01 13:00:52', '2013-07-01 13:00:52'),
	(101, 'Error', '2013-07-01 13:01:02', '2013-07-01 13:01:02'),
	(102, 'Error', '2013-07-01 13:01:18', '2013-07-01 13:01:18'),
	(103, 'Error', '2014-04-01 10:53:42', '2014-04-01 10:53:42'),
	(104, 'Error', '2014-04-01 10:53:45', '2014-04-01 10:53:45'),
	(105, 'Comprobar OK', '2014-04-01 10:53:53', '2014-04-01 10:53:53'),
	(106, 'Error', '2014-04-01 10:54:05', '2014-04-01 10:54:05'),
	(107, 'Error', '2014-04-01 10:54:21', '2014-04-01 10:54:21'),
	(108, 'Comprobar OK', '2014-04-01 10:54:28', '2014-04-01 10:54:28'),
	(109, 'Error', '2014-04-01 10:54:49', '2014-04-01 10:54:49'),
	(110, 'Comprobar OK', '2014-04-01 10:54:58', '2014-04-01 10:54:58'),
	(111, 'Error', '2014-04-01 10:56:19', '2014-04-01 10:56:19'),
	(112, 'Comprobar OK', '2014-04-01 10:56:27', '2014-04-01 10:56:27'),
	(113, 'Error', '2014-04-01 10:56:38', '2014-04-01 10:56:38'),
	(114, 'Error', '2014-04-01 10:56:44', '2014-04-01 10:56:44'),
	(115, 'Error', '2014-04-01 10:57:21', '2014-04-01 10:57:21'),
	(116, 'Comprobar OK', '2014-04-01 10:57:35', '2014-04-01 10:57:35'),
	(117, 'Error', '2014-04-01 11:01:40', '2014-04-01 11:01:40'),
	(118, 'Error', '2014-04-01 11:01:44', '2014-04-01 11:01:44'),
	(119, 'Error', '2014-04-01 11:01:55', '2014-04-01 11:01:55'),
	(120, 'Error', '2014-04-01 11:02:15', '2014-04-01 11:02:15'),
	(121, 'Comprobar OK', '2014-04-01 11:02:33', '2014-04-01 11:02:33'),
	(122, 'Error', '2014-04-01 11:02:47', '2014-04-01 11:02:47'),
	(123, 'Error', '2014-04-01 11:02:50', '2014-04-01 11:02:50'),
	(124, 'Error', '2014-04-01 11:02:51', '2014-04-01 11:02:51'),
	(125, 'Error', '2014-04-01 11:03:15', '2014-04-01 11:03:15'),
	(126, 'Jaula: 502B // Foto: 533a7ae34d025.jpeg', '2014-04-01 11:07:04', '2014-04-01 11:07:04'),
	(127, 'Jaula: 502B // Foto: 533a7ae34d025.jpeg', '2014-04-01 11:08:00', '2014-04-01 11:08:00'),
	(128, 'Jaula: 502B // Foto: 533a7ae34d025.jpeg', '2014-04-01 11:08:06', '2014-04-01 11:08:06'),
	(129, 'Jaula: 502B // Foto: 533a7ae34d025.jpeg', '2014-04-01 11:08:39', '2014-04-01 11:08:39'),
	(130, 'Jaula: 502B // Foto: 533a7ae34d025.jpeg', '2014-04-01 11:08:48', '2014-04-01 11:08:48'),
	(131, 'Jaula: 502A // Foto: 533a7adac4b20.jpeg', '2014-04-01 11:08:54', '2014-04-01 11:08:54'),
	(132, 'Jaula: 502A // Foto: 533a7adac4b20.jpeg', '2014-04-01 12:02:59', '2014-04-01 12:02:59');
/*!40000 ALTER TABLE `registro` ENABLE KEYS */;


-- Volcando estructura para tabla guikuzi.residente
DROP TABLE IF EXISTS `residente`;
CREATE TABLE IF NOT EXISTS `residente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apellido` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `codigo` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `codigodipu` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `codigonombre` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla guikuzi.residente: ~80 rows (aproximadamente)
/*!40000 ALTER TABLE `residente` DISABLE KEYS */;
INSERT INTO `residente` (`id`, `nombre`, `apellido`, `codigo`, `codigodipu`, `created_at`, `updated_at`, `codigonombre`) VALUES
	(1, 'Isabel', 'Gallastegui', NULL, 'D136', '2013-03-21 16:00:42', '2013-03-21 16:00:42', 'RIsabelGallastegui'),
	(2, 'Mª Ángeles', 'Viñe', NULL, NULL, '2013-03-21 16:01:00', '2013-03-21 16:01:00', NULL),
	(3, 'Felicidad', 'Martinez Alcocer', '23', 'D20', '2013-03-26 14:47:54', '2013-03-26 14:47:54', 'RFelicidadMartinez'),
	(4, 'Teresa', 'Gonzalez', NULL, '10', '2013-03-26 14:49:53', '2013-03-26 14:49:53', NULL),
	(5, 'Jose Manuel', 'Galvan', NULL, '76', '2013-04-16 16:09:17', '2013-04-16 16:09:17', NULL),
	(6, 'Beatriz', 'Ruiz', NULL, '131', '2013-04-16 16:09:39', '2013-04-16 16:09:39', NULL),
	(7, 'Jesusa', 'Barua', NULL, '23', '2013-04-16 16:09:58', '2013-04-16 16:09:58', NULL),
	(8, 'Casimiro', 'Quesada', NULL, '75', '2013-04-16 16:10:16', '2013-04-16 16:10:16', NULL),
	(9, 'Jose Ignacio', 'Ajuria', NULL, NULL, '2013-04-16 16:11:00', '2013-04-16 16:11:00', NULL),
	(10, 'Felipa', 'Maguregui', NULL, '118', '2013-04-16 16:11:18', '2013-04-16 16:11:18', NULL),
	(11, 'Esther', 'Muñoa', NULL, '151', '2013-04-16 16:11:38', '2013-04-16 16:11:38', NULL),
	(12, 'Josefa', 'Albizu', NULL, '41', '2013-04-16 16:11:56', '2013-04-16 16:11:56', NULL),
	(13, 'Rahma', 'Jebari', NULL, NULL, '2013-04-16 16:12:15', '2013-04-16 16:12:15', NULL),
	(14, 'Julio', 'Borrallo', NULL, '77', '2013-04-16 16:12:34', '2013-04-16 16:12:34', NULL),
	(15, 'Enrique', 'Añibarro', NULL, '93', '2013-04-16 16:12:52', '2013-04-16 16:12:52', NULL),
	(16, 'Maria', 'Aguirregomezcorta', NULL, '111', '2013-04-16 16:13:18', '2013-04-16 16:13:18', NULL),
	(17, 'Josefa', 'Ibarlaburu', NULL, '112', '2013-04-16 16:13:34', '2013-04-16 16:13:34', NULL),
	(18, 'Mª Isabel', 'Arevalo', NULL, '165', '2013-04-16 16:13:52', '2013-04-16 16:13:52', NULL),
	(19, 'Mª Luisa', 'Beitia', NULL, '126', '2013-04-16 16:14:14', '2013-04-16 16:14:14', NULL),
	(20, 'Visitacion', 'Pujante', NULL, '70', '2013-04-16 16:14:34', '2013-04-16 16:14:34', NULL),
	(21, 'Ines', 'Urresti', NULL, '139', '2013-04-16 16:14:53', '2013-04-16 16:14:53', NULL),
	(22, 'Agustina', 'Gallastegui', NULL, NULL, '2013-04-16 16:15:08', '2013-04-16 16:15:08', NULL),
	(23, 'Juana', 'Del Rivero', NULL, NULL, '2013-04-16 16:15:21', '2013-04-16 16:15:21', NULL),
	(24, 'Carmen', 'Mandia', NULL, '32', '2013-04-16 16:15:35', '2013-04-16 16:15:35', NULL),
	(25, 'Crescencia', 'Zabala', NULL, '123', '2013-04-16 16:15:50', '2013-04-16 16:15:50', NULL),
	(26, 'Isabel', 'Alonso', NULL, NULL, '2013-04-16 16:16:11', '2013-04-16 16:16:11', NULL),
	(27, 'Nieves', 'Perez', NULL, NULL, '2013-04-16 16:16:25', '2013-04-16 16:16:25', NULL),
	(28, 'Francisco', 'San Martin', NULL, NULL, '2013-04-16 16:16:37', '2013-04-16 16:16:37', NULL),
	(29, 'Jose', 'Maidagan', NULL, NULL, '2013-04-16 16:16:51', '2013-04-16 16:16:51', NULL),
	(30, 'Joaquina', 'Lezamiz', NULL, NULL, '2013-04-16 16:17:09', '2013-04-16 16:17:09', NULL),
	(31, 'Serafina', 'Alba', NULL, NULL, '2013-04-16 16:17:18', '2013-04-16 16:17:18', NULL),
	(32, 'Antonio', 'Alguacil', NULL, NULL, '2013-04-16 16:17:34', '2013-04-16 16:17:34', NULL),
	(33, 'Iñaki', 'Agirregoitia', NULL, '145', '2013-04-16 16:17:58', '2013-04-16 16:17:58', NULL),
	(34, 'Carmen', 'Rodriguez', NULL, '120', '2013-04-16 16:18:15', '2013-04-16 16:18:15', NULL),
	(35, 'Maria', 'Ibarrondo', NULL, NULL, '2013-04-16 16:18:34', '2013-04-16 16:18:34', NULL),
	(36, 'Jose', 'Ferreira', NULL, '181', '2013-04-16 16:18:48', '2013-04-16 16:18:48', NULL),
	(37, 'Natividad', 'Usieto', NULL, NULL, '2013-04-16 16:19:00', '2013-04-16 16:19:00', NULL),
	(38, 'Jesusa', 'Uriguen', NULL, NULL, '2013-04-16 16:19:20', '2013-04-16 16:19:20', NULL),
	(39, 'Maria', 'Sobrino', NULL, NULL, '2013-04-16 16:19:36', '2013-04-16 16:19:36', NULL),
	(40, 'Manuela', 'Benito', NULL, NULL, '2013-04-16 16:19:47', '2013-04-16 16:19:47', NULL),
	(41, 'Juana', 'Arzallus', NULL, '67', '2013-04-16 16:20:05', '2013-04-16 16:20:05', NULL),
	(42, 'Isabel', 'Garcia', NULL, NULL, '2013-04-16 16:20:15', '2013-04-16 16:20:15', NULL),
	(43, 'Mª Angeles', 'Aramendia', NULL, NULL, '2013-04-16 16:20:31', '2013-04-16 16:20:31', NULL),
	(44, 'Piedad', 'Muniozguren', NULL, NULL, '2013-04-16 16:20:45', '2013-04-16 16:20:45', NULL),
	(45, 'Mª Pilar', 'Dudagoitia', NULL, '155', '2013-04-16 16:21:05', '2013-04-16 16:21:05', NULL),
	(46, 'Martina', 'Uriarte', NULL, NULL, '2013-04-16 16:21:16', '2013-04-16 16:21:16', NULL),
	(47, 'Josefina', 'Irusta', NULL, NULL, '2013-04-16 16:21:27', '2013-04-16 16:21:27', NULL),
	(48, 'Mª Pilar', 'Garitagoitia', NULL, '124', '2013-04-16 16:21:45', '2013-04-16 16:21:45', NULL),
	(49, 'Mª Asuncion', 'Delgado', NULL, '203', '2013-04-16 16:22:03', '2013-04-16 16:22:03', NULL),
	(50, 'Milagros', 'Jauregui', NULL, '106', '2013-04-16 16:22:19', '2013-04-16 16:22:19', NULL),
	(51, 'Herminia', 'Tejera', NULL, NULL, '2013-04-16 16:22:31', '2013-04-16 16:22:31', NULL),
	(52, 'Concepcion', 'Larrinaga', NULL, NULL, '2013-04-16 16:22:47', '2013-04-16 16:22:47', NULL),
	(53, 'Maria', 'Alberdi', NULL, '44', '2013-04-16 16:23:00', '2013-04-16 16:23:00', NULL),
	(54, 'Guadalupe', 'Ocaranza', NULL, NULL, '2013-04-16 16:23:14', '2013-04-16 16:23:14', NULL),
	(55, 'Crescencio', 'Diez', NULL, '11', '2013-04-16 16:23:24', '2013-04-16 16:23:24', NULL),
	(56, 'Alejandro Marti', 'Ortiz De Zarate', NULL, NULL, '2013-04-16 16:23:58', '2013-04-16 16:23:58', NULL),
	(57, 'Benedicto', 'Garcia', NULL, NULL, '2013-04-16 16:24:10', '2013-04-16 16:24:10', NULL),
	(58, 'Mª Nieves', 'Gonzalez', NULL, NULL, '2013-04-16 16:24:21', '2013-04-16 16:24:21', NULL),
	(59, 'Teodora', 'Aranzabal', NULL, '22', '2013-04-16 16:24:35', '2013-04-16 16:24:35', NULL),
	(60, 'Mª Ignacia', 'Berregui', NULL, NULL, '2013-04-16 16:24:50', '2013-04-16 16:24:50', NULL),
	(61, 'Angela', 'Gorrochategui', NULL, '21', '2013-04-16 16:25:07', '2013-04-16 16:25:07', NULL),
	(62, 'Julian', 'Garcia', NULL, '83', '2013-04-16 16:25:18', '2013-04-16 16:25:18', NULL),
	(63, 'Mª Nieves', 'Berasaluce', NULL, NULL, '2013-04-16 16:25:33', '2013-04-16 16:25:33', NULL),
	(64, 'Guillermo', 'Otaegui', NULL, NULL, '2013-04-16 16:25:49', '2013-04-16 16:25:49', NULL),
	(65, 'Ramon', 'Castro', NULL, '64', '2013-04-16 16:26:03', '2013-04-16 16:26:03', NULL),
	(66, 'J. Antonio', 'Andikoetxea', NULL, '146', '2013-04-16 16:26:22', '2013-04-16 16:26:22', NULL),
	(67, 'Javier', 'Echeberria', NULL, NULL, '2013-04-16 16:26:53', '2013-04-16 16:26:53', NULL),
	(68, 'Manuel', 'Garcia', NULL, NULL, '2013-04-16 16:27:57', '2013-04-16 16:27:57', NULL),
	(69, 'Juan Luis', 'Ochandiano', NULL, NULL, '2013-04-16 16:28:21', '2013-04-16 16:28:21', NULL),
	(70, 'Balbina', 'Larringan', NULL, NULL, '2013-04-16 16:28:34', '2013-04-16 16:28:34', NULL),
	(71, 'Mª Teresa', 'Iciar', NULL, '24', '2013-04-16 16:28:53', '2013-04-16 16:28:53', NULL),
	(72, 'Carmen', 'Areta', NULL, NULL, '2013-04-16 16:29:08', '2013-04-16 16:29:08', NULL),
	(73, 'Emiliana', 'Asurmendi', NULL, '301', '2013-04-16 16:29:26', '2013-04-16 16:29:26', NULL),
	(74, 'Consuelo', 'Ederra', NULL, NULL, '2013-04-16 16:29:36', '2013-04-16 16:29:36', NULL),
	(75, 'Celia', 'Gonzalez', NULL, '94', '2013-04-16 16:29:50', '2013-04-16 16:29:50', NULL),
	(76, 'Eugenio', 'Izaguirre', NULL, '108', '2013-04-16 16:30:08', '2013-04-16 16:30:08', NULL),
	(77, 'Marcelina', 'Hernandez', NULL, '209', '2013-04-16 16:30:24', '2013-04-16 16:30:24', NULL),
	(78, 'Lucia', 'Lamarain', NULL, NULL, '2013-04-16 16:30:37', '2013-04-16 16:30:37', NULL),
	(79, 'Victoria', 'Lamarain', NULL, NULL, '2013-04-16 16:30:46', '2013-04-16 16:30:46', NULL),
	(80, 'Jesus', 'Ayestaran', 'JesusAyest', 'D109', '2014-04-01 10:47:43', '2014-04-01 10:47:43', 'R109');
/*!40000 ALTER TABLE `residente` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
