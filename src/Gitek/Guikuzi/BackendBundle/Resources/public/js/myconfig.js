/**
 * Created by ikerib on 11/06/14.
 */

$("#selres").on("click", function () {
    var hab;
    hab = this.value;
    $("#gitek_guikuzi_backendbundle_configtype_residente option:selected").prop("selected", false);
    $("#gitek_guikuzi_backendbundle_configtype_residente option").filter(function () {
        return this.value === hab;
    }).attr("selected", true);
    return $("#gitek_guikuzi_backendbundle_configtype_residente").val(hab);
});


$("#seljau").on("click", function () {
    var jaulaid, jaulatext, that, url;
    if (!this.value) {
        return false;
    }
    that = this;
    miarray = [];
    jaulaid = this.value;
    if (!jaulaid) {
        return alert("Selecciona Jaula");
    }

    url = Routing.generate("api_gelak", {
        jaulaid: this.value
    });
    $.getJSON(url, function (json) {
        var dato, i=0, midato, _results;

        $('#selhab').empty();

        while (i < json.length) {
            dato = json[i];
            midato = {
                id: dato.id,
                foto: dato.nombre
            };

            $('#selhab').append('<option value=' + dato.id + '>' + dato.descripcion + '</option>');

            i++;
        }
        $('#gitek_guikuzi_backendbundle_configtype_jaula option:selected').prop("selected", false);
        $('#gitek_guikuzi_backendbundle_configtype_jaula option').filter(function () {
            return this.value === jaulaid;
        }).attr("selected", true);
        $('#gitek_guikuzi_backendbundle_configtype_jaula').val(jaulaid);
        jaulatext = $("#gitek_guikuzi_backendbundle_configtype_jaula option:selected").text();
    });

});


$("#selhab").on('click', function () {
    var hab, habtext, miruta;
    hab = this.value;
    if (!hab) {
        return alert("Selecciona habitacion!");
    }
    $("#gitek_guikuzi_backendbundle_configtype_jauladet option:selected").prop("selected", false);
    $("#gitek_guikuzi_backendbundle_configtype_jauladet option").filter(function () {
        return this.value === hab;
    }).attr("selected", true);
    $("#gitek_guikuzi_backendbundle_configtype_jauladet").val(hab);
    habtext = $("#gitek_guikuzi_backendbundle_configtype_jauladet option:selected").text();
    $('.tdcodbar').append("<span>Habitacion: " + habtext + "</span>");
//    miruta = $('#miruta').val();
//    if (miruta === "admin_config_edit") {
//        return alert("Has cambiado la  habitación. Acuerdate de cambiar el hueco correspondiente.");
//    }
});

$( document ).ready(function () {

    var nirejauladetid = $('#gitek_guikuzi_backendbundle_configtype_jauladet').val();
    var nirejauladettext = $('#gitek_guikuzi_backendbundle_configtype_jauladet option:selected').text();

    $('#selhab').append('<option selected value=' + nirejauladetid + '>' + nirejauladettext + '</option>');

});

$(function () {
    return $("#selres").filterByText($("#findres"), true);
});

jQuery.fn.filterByText = function (textbox, selectSingleMatch) {
    return this.each(function () {
        var options, select;
        select = this;
        options = [];
        $(select).find("option").each(function () {
            return options.push({
                value: $(this).val(),
                text: $(this).text()
            });
        });
        $(select).data("options", options);
        return $(textbox).bind("change keyup", function () {
            var regex, search;
            options = $(select).empty().scrollTop(0).data("options");
            search = $.trim($(this).val());
            regex = new RegExp(search, "gi");
            $.each(options, function (i) {
                var option;
                option = options[i];
                if (option.text.match(regex) !== null) {
                    $(select).append($("<option>").text(option.text).val(option.value));
                }
            });
            if (selectSingleMatch === true && $(select).children().length === 1) {
                $(select).children().get(0).selected = true;
                $("#selres").click();
            }
        });
    });
};