<?php

namespace Gitek\Guikuzi\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use Gitek\Guikuzi\BackendBundle\Entity\Registro;
use Gitek\Guikuzi\BackendBundle\Form\RegistroType;


class RegistroController extends Controller
{

    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BackendBundle:Registro')->findAll();

        return $this->render('BackendBundle:Registro:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    public function grafikoakAction()
    {
        $em = $this->getDoctrine()->getManager();

        $datuak = $em->getRepository('BackendBundle:Registro')->getdatuak(null, null);
        $erantzuna = $this->moldatudatuak($datuak);
        $serializador = $this->container->get('serializer');

        return $this->render('BackendBundle:Registro:grafikoak.html.twig', array(
            'datuak' => $erantzuna,
        ));
    }

    public function chartAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $desde = $request->request->get('desde');
        $hasta = $request->request->get('hasta');
        $datuak = $em->getRepository('BackendBundle:Registro')->getdatuak($desde, $hasta);

        $erantzuna = $this->moldatudatuak($datuak);

        $serializador = $this->container->get('serializer');

        return new Response($serializador->serialize($erantzuna, 'json'));
    }

    public function chartinfoAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $desde = $request->request->get('desde');
        $hasta = $request->request->get('hasta');
        $datuak = $em->getRepository('BackendBundle:Registro')->getdatuakinfo($desde, $hasta);
        $erantzuna = $this->moldatudatuak($datuak);
        $serializador = $this->container->get('serializer');

        return new Response($serializador->serialize($erantzuna, 'json'));
    }


    function moldatudatuak ($datuak){

        $koloreak = array(0=>'#F38630',1=>'#584A5E',2=>'#69D2E7',3=>'#468847');
        $erantzuna = array();

        foreach($datuak as $key => $value)
        {
            $color ="";
            if ( $key > 3) {
                $color = "#5e9ab4";
            } else {
                $color = $koloreak[$key];
            }

            if ($value['resultado'] == ""){
                $temp = array('value'=> $value['Zenbat'], 'color'=>"$color", 'descripcion' => 'Error de sistema');
            } else {
                $temp = array('value'=> $value['Zenbat'], 'color'=>"$color", 'descripcion' => $value['descripcion']);
            }
            if ($value['resultado'] == "OK"){
                $temp = array('value'=> $value['Zenbat'], 'color'=>"$color", 'descripcion' => "OK");
            }

            array_push($erantzuna, $temp);
        }

        return $erantzuna;
    }
}
