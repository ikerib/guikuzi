<?php

namespace Gitek\Guikuzi\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Gitek\Guikuzi\BackendBundle\Entity\Jauladet;
use Gitek\Guikuzi\BackendBundle\Form\JauladetType;

/**
 * Jauladet controller.
 *
 */
class JauladetController extends Controller
{
    /**
     * Lists all Jauladet entities.
     *
     */
    public function indexAction()
    {
      $em = $this->getDoctrine()->getManager();

      $entities = $em->getRepository('BackendBundle:Jauladet')->findAll();

      return $this->render('BackendBundle:Jauladet:index.html.twig', array(
        'entities' => $entities,
        ));
    }

    /**
     * Creates a new Jauladet entity.
     *
     */
    public function createAction(Request $request)
    {
      $entity  = new Jauladet();
      $form = $this->createForm(new JauladetType(), $entity);
      $form->bind($request);

      if ($form->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('admin_jaula_edit', array('id' => $entity->getJaula()->getId())));
      }

      return $this->render('BackendBundle:Jauladet:new.html.twig', array(
        'entity' => $entity,
        'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Jauladet entity.
     *
     */
    public function newAction($jaulaid)
    {
      $em = $this->getDoctrine()->getManager();
      $jaula = $em->getRepository('BackendBundle:Jaula')->find($jaulaid);

      $entity = new Jauladet();
      $entity->setJaula($jaula);

      $form   = $this->createForm(new JauladetType(), $entity);

      return $this->render('BackendBundle:Jauladet:new.html.twig', array(
        'entity' => $entity,
        'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Jauladet entity.
     *
     */
    public function showAction($id)
    {
      $em = $this->getDoctrine()->getManager();

      $entity = $em->getRepository('BackendBundle:Jauladet')->find($id);

      if (!$entity) {
        throw $this->createNotFoundException('Unable to find Jauladet entity.');
      }

      $deleteForm = $this->createDeleteForm($id);

      return $this->render('BackendBundle:Jauladet:show.html.twig', array(
        'entity'      => $entity,
        'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Jauladet entity.
     *
     */
    public function editAction($id)
    {
      $em = $this->getDoctrine()->getManager();

      $entity = $em->getRepository('BackendBundle:Jauladet')->find($id);

      if (!$entity) {
        throw $this->createNotFoundException('Unable to find Jauladet entity.');
      }

      $editForm = $this->createForm(new JauladetType(), $entity);
      $deleteForm = $this->createDeleteForm($id);

      return $this->render('BackendBundle:Jauladet:edit.html.twig', array(
        'entity'      => $entity,
        'edit_form'   => $editForm->createView(),
        'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Jauladet entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
      $em = $this->getDoctrine()->getManager();

      $entity = $em->getRepository('BackendBundle:Jauladet')->find($id);

      if (!$entity) {
        throw $this->createNotFoundException('Unable to find Jauladet entity.');
      }

      $deleteForm = $this->createDeleteForm($id);
      $editForm = $this->createForm(new JauladetType(), $entity);
      $editForm->bind($request);

      if ($editForm->isValid()) {
        $entity->setUpdatedAt(new \DateTime());
        $em->persist($entity);
        $em->flush();

            // return $this->redirect($this->generateUrl('admin_jauladet_edit', array('id' => $id)));
        return $this->redirect($this->generateUrl('admin_jaula_edit', array('id' => $entity->getJaula()->getId())));
      }

      return $this->render('BackendBundle:Jauladet:edit.html.twig', array(
        'entity'      => $entity,
        'edit_form'   => $editForm->createView(),
        'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Jauladet entity.
     *
     */
    public function deleteAction($id, $token)
    {
      $em = $this->getDoctrine()->getManager();
      $entity = $em->getRepository('BackendBundle:Jauladet')->find($id);
      if (!$entity) {
        throw $this->createNotFoundException('Unable to find jauladet entity.');
      }
      $jauladetid = $entity->getJaula()->getId();
      $csrf  = $this->container->get('form.csrf_provider');

      if ($csrf->isCsrfTokenValid($entity->getCsrfIntention('delete'), $token)) {
        $em->remove($entity);
        $em->flush();
        $this->container->get('lrotherfield.notify')->add("delete", array("message" => "Eliminado con exito."));
      } else {
        throw $this->createNotFoundException('Token es incorrecto.');
      }

      return $this->redirect($this->generateUrl('admin_jaula_edit', array('id' => $jauladetid)));

    }

    /**
     * Creates a form to delete a Jauladet entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
      return $this->createFormBuilder(array('id' => $id))
      ->add('id', 'hidden')
      ->getForm()
      ;
    }
  }
