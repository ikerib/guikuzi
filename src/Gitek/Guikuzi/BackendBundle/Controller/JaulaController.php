<?php

namespace Gitek\Guikuzi\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Gitek\Guikuzi\BackendBundle\Entity\Jaula;
use Gitek\Guikuzi\BackendBundle\Entity\Jauladet;
use Gitek\Guikuzi\BackendBundle\Form\JaulaType;
use Gitek\Guikuzi\BackendBundle\Form\JauladetType;

/**
 * Jaula controller.
 *
 */
class JaulaController extends Controller
{
    /**
     * Lists all Jaula entities.
     *
     */
    public function indexAction()
    {
      $em = $this->getDoctrine()->getManager();

      $entities = $em->getRepository('BackendBundle:Jaula')->findAll();

      return $this->render('BackendBundle:Jaula:index.html.twig', array(
        'entities' => $entities,
        ));
    }

    /**
     * Creates a new Jaula entity.
     *
     */
    public function createAction(Request $request)
    {
      $entity  = new Jaula();
      $form = $this->createForm(new JaulaType(), $entity);
      $form->bind($request);

      if ($form->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();

        $this->container->get('lrotherfield.notify')->add("nuevo", array("message" => "Jaula creada. Especifique huecos ahora."));

//        return $this->redirect($this->generateUrl('admin_jaula_show', array('id' => $entity->getId())));
          return $this->redirect($this->generateUrl('admin_jaula_edit', array('id' => $entity->getId())));
      }

      return $this->render('BackendBundle:Jaula:new.html.twig', array(
        'entity' => $entity,
        'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Jaula entity.
     *
     */
    public function newAction()
    {
      $entity = new Jaula();
      $form   = $this->createForm(new JaulaType(), $entity);

      return $this->render('BackendBundle:Jaula:new.html.twig', array(
        'entity' => $entity,
        'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Jaula entity.
     *
     */
    public function showAction($id)
    {
      $em = $this->getDoctrine()->getManager();

      $entity = $em->getRepository('BackendBundle:Jaula')->find($id);

      if (!$entity) {
        throw $this->createNotFoundException('Unable to find Jaula entity.');
      }

      $deleteForm = $this->createDeleteForm($id);

      return $this->render('BackendBundle:Jaula:show.html.twig', array(
        'entity'      => $entity,
        'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Jaula entity.
     *
     */
    public function editAction($id)
    {
      $em = $this->getDoctrine()->getManager();

      $entity = $em->getRepository('BackendBundle:Jaula')->find($id);

      if (!$entity) {
        throw $this->createNotFoundException('Unable to find Jaula entity.');
      }

      $editForm = $this->createForm(new JaulaType(), $entity);
      $deleteForm = $this->createDeleteForm($id);

      return $this->render('BackendBundle:Jaula:edit.html.twig', array(
        'entity'      => $entity,
        'edit_form'   => $editForm->createView(),
        'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Jaula entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
      $em = $this->getDoctrine()->getManager();

      $jaula = $em->getRepository('BackendBundle:Jaula')->find($id);

      if (!$jaula) {
        throw $this->createNotFoundException('Unable to find Jaula entity.');
      }


      $originalJauladets = array();

        // Create an array of the current Tag objects in the database
      foreach ($jaula->getJauladets() as $jauladet) {
        $originalJauladets[] = $jauladet;
      }


      $editForm = $this->createForm(new JaulaType(), $jaula);

      $editForm->bind($this->getRequest());

        // formularioko datuak hartzeitugu eta zerbait aldatzen dugu
        // igoera egin dezan
      $data = $editForm->getData();
      $misdatos = $data->getJauladets();
      foreach ($misdatos as $dato) {
        // $miimagen = 'images/uploads/' . $dato->getFoto();
        // ladybug_dump( $dato );
        // ladybug_dump( $miimagen );
        // $imagemanagerResponse = $this->container
        //       ->get('liip_imagine.controller')
        //           ->filterAction(
        //               $this->getRequest(),
        //               $miimagen,      // original image you want to apply a filter to
        //               'my_prenda'              // filter defined in config.yml
        //   );

        //   $imagemanagerResponse = $this->container
        //       ->get('liip_imagine.controller')
        //           ->filterAction(
        //               $this->getRequest(),
        //               $miimagen,      // original image you want to apply a filter to
        //               'my_prenda'              // filter defined in config.yml
        //   );
        $dato->setUpdatedAt(new \DateTime());
        $em->persist($dato);
      }
      if ($editForm->isValid()) {
        // filter $originalJauladets to contain Jauladets no longer present
        foreach ($jaula->getJauladets() as $jauladet) {
          foreach ($originalJauladets as $key => $toDel) {
            if ($toDel->getId() === $jauladet->getId()) {
              // ladybug_dump( "JUJUJU" );
              unset($originalJauladets[$key]);
            }
          }
        }

        // remove the relationship between the tag and the Jaula
        foreach ($originalJauladets as $jauladet) {
          // remove the Jaula from the Tag
          // print_r(" hemen ");
          // $jauladet->getJaula()->removeElement($jaula);
          // $jauladet->getJaula()->removeJauladet($jauladet);
          // print_r(" jajajajjaj ");
                // if it were a ManyToOne relationship, remove the relationship like this
          $jauladet->setUpdatedAt(new \DateTime());
          $jauladet->setJaula(null);

          $em->persist($jauladet);
          $em->remove($jauladet);

                // if you wanted to delete the Tag entirely, you can also do that

        }

        $em->persist($jaula);
        $em->flush();

      }

      return $this->redirect($this->generateUrl('admin_jaula_edit', array('id' => $id)));

    }

    /**
     * Deletes a Jaula entity.
     *
     */
    public function deleteAction($id, $token)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Jaula')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find jaula entity.');
        }
        $csrf  = $this->container->get('form.csrf_provider');

        if ($csrf->isCsrfTokenValid($entity->getCsrfIntention('delete'), $token)) {
            $em->remove($entity);
            $em->flush();
            $this->container->get('lrotherfield.notify')->add("delete", array("message" => "Eliminado con exito."));
        } else {
            throw $this->createNotFoundException('Token es incorrecto.');
        }

        return $this->redirect($this->generateUrl('admin_jaula'));

    }

    /**
     * Creates a form to delete a Jaula entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
      return $this->createFormBuilder(array('id' => $id))
      ->add('id', 'hidden')
      ->getForm()
      ;
    }
  }
