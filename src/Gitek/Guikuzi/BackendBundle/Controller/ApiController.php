<?php

namespace Gitek\Guikuzi\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

use FOS\RestBundle\View\View;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Symfony\Component\HttpFoundation\Session\Session;

use Gitek\Guikuzi\BackendBundle\Entity\Registro;

class ApiController extends Controller
{

    public function bilaketaAction($codbar)
    {

        $em = $this->getDoctrine()->getManager();
        $session = $this->getRequest()->getSession();
        /* Miramos si tiene variable de session,
         * si tiene comprueba que es la jaula correcta
         * sino muestra el hueco
         */
        $orain = 0;
        if (($session->get('comprobar') == null) or ($session->get('comprobar') == "0")) { // Sin Comprobar

            $jaula = $em->getRepository('BackendBundle:Jaula')->jaulaporcodbar($codbar);
            if ($jaula) {
                if ($jaula->getJaula()->getComprobar() == 1) {
                    $session->set('comprobar', "1");
                    $session->set('codbar',$codbar);
                    $orain = 1;
                } else {
                    $session->set('comprobar', "0");
                    $session->set('codbar',"");
                }
            } else {
                $registro = New Registro();
                $jaula = null;
                $erantzuna = array(
                    "miimagen" => "/bundles/frontend/img/error.jpg", 
                    "minombre" => "No encontrado.",
                    "mp3" => "noencontrado.mp3",
                    "completed"=> "0");
                $registro->setResultado("KO1");
                $registro->setDescripcion("Error, no encontrado");
                $em->persist($registro);
                $em->flush();

                $serializador = $this->container->get('serializer');

                return new Response($serializador->serialize($erantzuna, 'json'));
            }
        } else { // Comprobación
            //$jaula = $em->getRepository('BackendBundle:Jaula')->compruebacodbar($codbar);
            $residente = $session->get('codbar');
            $jaula = $em->getRepository('BackendBundle:Jaula')->compruebacodbarhueco($residente,$codbar);
            /*
            * Si no encuentra, comprobamos que lo que a introducido es un codbar de habitacion/residente
            * y si lo es indicamos que esperamos un codbar de hueco
            */
            if (!$jaula) {
                $jaula = $em->getRepository('BackendBundle:Jaula')->jaulaporcodbar($codbar);
                if ($jaula) {
                    $registro = New Registro();

                    $res = $em->getRepository('BackendBundle:Residente')->findOneBy(array('codigo'=>$residente));

                    $jaula = null;
                    $erantzuna = array(
                        "miimagen" => "/bundles/frontend/img/error.jpg", 
                        "minombre" => "OJO se esperaba lectura de hueco. Residente: " . $res->getNombre() . " " . $res->getApellido(),
                        "mp3" => "seesperabalecturadehueco.mp3",
                        "completed"=> "0"
                        );
                    $registro->setResultado("KO2");
                    $registro->setDescripcion("Error, se esperaba lectura de hueco");
                    $em->persist($registro);
                    $em->flush();

                    $serializador = $this->container->get('serializer');

                    return new Response($serializador->serialize($erantzuna, 'json'));
                }
            }
        }

        $erantzuna = "";
        $registro = New Registro();

        if (!$jaula) {
            $jaula = null;
            $res = $em->getRepository('BackendBundle:Residente')->findOneBy(array('codigo'=>$residente));
            $erantzuna = array(
                "miimagen" => "/bundles/frontend/img/error.jpg",
                "minombre" => "Hueco incorrecto. Residente: " . $res->getNombre() . " " . $res->getApellido(),
                "mp3" => "incorrecto.mp3"
            );
            $registro->setResultado("KO3");
            $registro->setDescripcion("Error, hueco incorrecto");
            $em->persist($registro);
            $em->flush();
        } else {
            if (($session->get('comprobar') == "1") && ($orain == 0)) {
                $session->set('comprobar', "0");
                $session->set('codbar','');
                $res = $em->getRepository('BackendBundle:Residente')->findOneBy(array('codigo'=>$residente));
                $erantzuna = array(
                        "miimagen" => "/bundles/frontend/img/ok.jpg", 
                        "minombre" => "Hueco correcto. Residente: " . $res->getNombre() . " " . $res->getApellido(),
                        "mp3" => "",
                        "completed"=> "1");
                $registro->setResultado("OK");
                $registro->setDescripcion("Comprobar OK");
                $em->persist($registro);
                $em->flush();
            } else {
                $miimagen = 'images/uploads/' . $jaula->getFoto();
                $imagePath = $miimagen; // ¿Porque hice thumbs?
                $erantzuna = array(
                    "miimagen" => $imagePath, 
                    "minombre" => $jaula->getDescripcion(), 
                    "completed"=> "0",
                    "mijaula" => $jaula);
                $registro->setResultado("OK");
                $registro->setDescripcion("Jaula: " . $jaula->getNombre() . " // Foto: " . $jaula->getFoto());
                $em->persist($registro);
                $em->flush();
            }
        }

        $serializador = $this->container->get('serializer');

        return new Response($serializador->serialize($erantzuna, 'json'));

    }

    public function getirudiakAction($jaulaid)
    {
        $em = $this->getDoctrine()->getManager();

        $erantzuna = $em->getRepository('BackendBundle:Jaula')->bilatuirudiak($jaulaid);

        if (!$erantzuna) {

            $erantzuna = null;

        }

        $serializador = $this->container->get('serializer');

        return new Response($serializador->serialize($erantzuna, 'json'));
    }

    public function getgelakAction($jaulaid)
    {
        $em = $this->getDoctrine()->getManager();

        $erantzuna = $em->getRepository('BackendBundle:Jaula')->bilatugelak($jaulaid);

        if (!$erantzuna) {

            $erantzuna = null;

        }

        $serializador = $this->container->get('serializer');

        return new Response($serializador->serialize($erantzuna, 'json'));
    }


}
