<?php

namespace Gitek\Guikuzi\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Gitek\Guikuzi\BackendBundle\Entity\Config;
use Gitek\Guikuzi\BackendBundle\Entity\Jauladet;
use Gitek\Guikuzi\BackendBundle\Form\ConfigType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;


use Gitek\Guikuzi\BackendBundle\Form\JauladetType;

/**
 * Config controller.
 *
 */
class ConfigController extends Controller
{

    public function konfigurazioaAction()
    {
        $em = $this->getDoctrine()->getManager();

        $config = new Config();
        $form = $this->createForm(new ConfigType(), $config);

        $centro = $this->container->getParameter('gitek.centro_por_defecto');
        $residentes = $em->getRepository('BackendBundle:Residente')->findAll();
        $jaulas = $em->getRepository('BackendBundle:Jaula')->findAll();
        $habitaciones = $em->getRepository('BackendBundle:Habitacion')->bilatugelak();
        $irudiak = null;
        $miconfig = null; // para edit

        return $this->render('BackendBundle:Config:konfigurazioa.html.twig', array(
            'centro' => $centro,
            'residentes' => $residentes,
            'jaulas' => $jaulas,
            'habitaciones' => $habitaciones,
            'miconfig' => $miconfig,
            'irudiak' => $irudiak,
            'form' => $form->createView(),
        ));
    }

    public function getjauladetsAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $jauladets = $em->getRepository('BackendBundle:Jauladet')->findByJaula($id);
        if (!$jauladets) {
            throw $this->createNotFoundException('Unable to find $jauladets entity.');
        }

        $serializador = $this->container->get('serializer');
        return new Response($serializador->serialize($jauladets, 'json'));
    }

    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BackendBundle:Config')->findAll();

        return $this->render('BackendBundle:Config:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    public function createAction(Request $request)
    {
        $entity = new Config();
        $form = $this->createForm(new ConfigType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->container->get('lrotherfield.notify')->add("nuevo", array("message" => "Datos grabados con exito."));

            return $this->redirect($this->generateUrl('admin_config'));
        }

        return $this->render('BackendBundle:Config:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $miconfig = $em->getRepository('BackendBundle:Config')->find($id);

        if (!$miconfig) {
            throw $this->createNotFoundException('Unable to find Config entity.');
        }

        $form = $this->createForm(new ConfigType(), $miconfig);
        $deleteForm = $this->createDeleteForm($id);
        $centro = $this->container->getParameter('gitek.centro_por_defecto');
        $residentes = $em->getRepository('BackendBundle:Residente')->findAll();
        $jaulas = $em->getRepository('BackendBundle:Jaula')->findAll();
        $habitaciones = $em->getRepository('BackendBundle:Habitacion')->findAll();
        $irudiak = $em->getRepository('BackendBundle:Jaula')->bilatuirudiak($miconfig->getJaula()->getId());

        return $this->render('BackendBundle:Config:konfigurazioa.html.twig', array(
            'miconfig' => $miconfig,
            'centro' => $centro,
            'residentes' => $residentes,
            'jaulas' => $jaulas,
            'habitaciones' => $habitaciones,
            'irudiak' => $irudiak,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Config')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Config entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new ConfigType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->container->get('lrotherfield.notify')->add("nuevo", array("message" => "Datos grabados con exito."));

            // return $this->redirect($this->generateUrl('admin_config'));
            return $this->redirect($this->generateUrl('admin_config'));
        }

        return $this->render('BackendBundle:Config:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function deleteAction($id, $token)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Config')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Config entity.');
        }
        $csrf = $this->container->get('form.csrf_provider');

        if ($csrf->isCsrfTokenValid($entity->getCsrfIntention('delete'), $token)) {
            $em->remove($entity);
            $em->flush();
        } else {
            throw $this->createNotFoundException('Token es incorrecto.');
        }

        return $this->redirect($this->generateUrl('admin_config'));
    }

    /**
     * Creates a form to delete a Config entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm();
    }
}