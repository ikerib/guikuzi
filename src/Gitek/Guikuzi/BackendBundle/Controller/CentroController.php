<?php

namespace Gitek\Guikuzi\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Gitek\Guikuzi\BackendBundle\Entity\Centro;
use Gitek\Guikuzi\BackendBundle\Form\CentroType;
use Gitek\Guikuzi\BackendBundle\Form\SeleccionType;

/**
 * Centro controller.
 *
 */
class CentroController extends Controller
{
    public function seleccionAction(Request $request)
    {
      $entity  = new Centro();
      $form = $this->createForm(new SeleccionType(), $entity);
      $centro = $this->container->getParameter('gitek.centro_por_defecto');

      if ($this->getRequest()->getMethod() == 'POST') {
        $form->bind($this->getRequest());
        if ($form->isValid()) {
          $miseleccion = $request->request->get('selecciontype');
          $this->container->setParameter('gitek.centro_por_defecto', $miseleccion['nombre']);

          // return $this->redirect($this->generateUrl('admin_centro'));
        }
      }

      return $this->render('BackendBundle:Centro:seleccion.html.twig', array(
        'entity' => $entity,
        'form'   => $form->createView(),
        ));
    }

    /**
     * Lists all Centro entities.
     *
     */
    public function indexAction()
    {
      $em = $this->getDoctrine()->getManager();

      $entities = $em->getRepository('BackendBundle:Centro')->findAll();

      return $this->render('BackendBundle:Centro:index.html.twig', array(
        'entities' => $entities,
        ));
    }

    /**
     * Creates a new Centro entity.
     *
     */
    public function createAction(Request $request)
    {
      $entity  = new Centro();
      $form = $this->createForm(new CentroType(), $entity);
      $form->bind($request);

      if ($form->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();

            // return $this->redirect($this->generateUrl('centro_show', array('id' => $entity->getId())));
        return $this->redirect($this->generateUrl('admin_centro_index'));
      }

      return $this->render('BackendBundle:Centro:new.html.twig', array(
        'entity' => $entity,
        'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Centro entity.
     *
     */
    public function newAction()
    {
      $entity = new Centro();
      $form   = $this->createForm(new CentroType(), $entity);

      return $this->render('BackendBundle:Centro:new.html.twig', array(
        'entity' => $entity,
        'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Centro entity.
     *
     */
    public function showAction($id)
    {
      $em = $this->getDoctrine()->getManager();

      $entity = $em->getRepository('BackendBundle:Centro')->find($id);

      if (!$entity) {
        throw $this->createNotFoundException('Unable to find Centro entity.');
      }

      $deleteForm = $this->createDeleteForm($id);

      return $this->render('BackendBundle:Centro:show.html.twig', array(
        'entity'      => $entity,
        'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Centro entity.
     *
     */
    public function editAction($id)
    {
      $em = $this->getDoctrine()->getManager();

      $entity = $em->getRepository('BackendBundle:Centro')->find($id);

      if (!$entity) {
        throw $this->createNotFoundException('Unable to find Centro entity.');
      }

      $editForm = $this->createForm(new CentroType(), $entity);
      $deleteForm = $this->createDeleteForm($id);

      return $this->render('BackendBundle:Centro:edit.html.twig', array(
        'entity'      => $entity,
        'edit_form'   => $editForm->createView(),
        'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Centro entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
      $em = $this->getDoctrine()->getManager();

      $entity = $em->getRepository('BackendBundle:Centro')->find($id);

      if (!$entity) {
        throw $this->createNotFoundException('Unable to find Centro entity.');
      }

      $deleteForm = $this->createDeleteForm($id);
      $editForm = $this->createForm(new CentroType(), $entity);
      $editForm->bind($request);

      if ($editForm->isValid()) {
        $em->persist($entity);
        $em->flush();

            // return $this->redirect($this->generateUrl('centro_edit', array('id' => $id)));
        return $this->redirect($this->generateUrl('admin_centro_index'));
      }

      return $this->render('BackendBundle:Centro:edit.html.twig', array(
        'entity'      => $entity,
        'edit_form'   => $editForm->createView(),
        'delete_form' => $deleteForm->createView(),
        ));
    }

    public function deleteAction($id, $token)
    {
      $em = $this->getDoctrine()->getManager();
      $entity = $em->getRepository('BackendBundle:Centro')->find($id);
      if (!$entity) {
        throw $this->createNotFoundException('Unable to find Centro entity.');
      }
      $csrf  = $this->container->get('form.csrf_provider');

      if ($csrf->isCsrfTokenValid($entity->getCsrfIntention('delete'), $token)) {
        $em->remove($entity);
        $em->flush();
      } else {
            throw $this->createNotFoundException('Token es incorrecto.');
      }

      return $this->redirect($this->generateUrl('admin_centro_index'));

    }
    // /**
    //  * Deletes a Centro entity.
    //  *
    //  */
    // public function deleteAction(Request $request, $id)
    // {
    //   $form = $this->createDeleteForm($id);
    //   $form->bind($request);

    //   if ($form->isValid()) {
    //     $em = $this->getDoctrine()->getManager();
    //     $entity = $em->getRepository('BackendBundle:Centro')->find($id);

    //     if (!$entity) {
    //       throw $this->createNotFoundException('Unable to find Centro entity.');
    //     }

    //     $em->remove($entity);
    //     $em->flush();
    //   }

    //   return $this->redirect($this->generateUrl('admin_centro'));
    // }

    /**
     * Creates a form to delete a Centro entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
      return $this->createFormBuilder(array('id' => $id))
      ->add('id', 'hidden')
      ->getForm()
      ;
    }
  }
