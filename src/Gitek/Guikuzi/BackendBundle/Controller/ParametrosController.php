<?php

namespace Gitek\Guikuzi\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Gitek\Guikuzi\BackendBundle\Entity\Parametros;
use Gitek\Guikuzi\BackendBundle\Form\ParametrosType;

class ParametrosController extends Controller
{

    public function indexAction()
    {
      $em = $this->getDoctrine()->getManager();

      $parametros = $em->getRepository('BackendBundle:Parametros')->find(1);

      if (count($parametros)==0) {
        $parametros = New Parametros();
        $em->persist($parametros);
        $em->flush();
      }

      if ($this->getRequest()->getMethod() == 'POST') {

        $form = $this->createForm(new ParametrosType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
          $em->persist($parametros);
          $em->flush();
          print_r("HEMEN");
          return $this->redirect($this->generateUrl('admin_centro_index'));
        }
      } else {
        $form = $this->createForm(new ParametrosType(), $parametros);

        return $this->render('BackendBundle:Parametros:index.html.twig', array(
          'parametros'  => $parametros,
          'form'        => $form->createView(),
          ));
      }
    }
}
