<?php

namespace Gitek\Guikuzi\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Gitek\Guikuzi\BackendBundle\Entity\Habitacion;
use Gitek\Guikuzi\BackendBundle\Form\HabitacionType;

/**
 * Habitacion controller.
 *
 */
class HabitacionController extends Controller
{
    /**
     * Lists all Habitacion entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BackendBundle:Habitacion')->findAll();

        return $this->render('BackendBundle:Habitacion:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new Habitacion entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Habitacion();
        $form = $this->createForm(new HabitacionType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->container->get('lrotherfield.notify')->add("index",array("message" => "Guardado con exito."));

            // return $this->redirect($this->generateUrl('admin_habitacion_show', array('id' => $entity->getId())));
            return $this->redirect($this->generateUrl('admin_habitacion'));
        }

        return $this->render('BackendBundle:Habitacion:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Habitacion entity.
     *
     */
    public function newAction()
    {
        $entity = new Habitacion();
        $form   = $this->createForm(new HabitacionType(), $entity);

        return $this->render('BackendBundle:Habitacion:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Habitacion entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Habitacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Habitacion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BackendBundle:Habitacion:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Habitacion entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Habitacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Habitacion entity.');
        }

        $editForm = $this->createForm(new HabitacionType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BackendBundle:Habitacion:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Habitacion entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Habitacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Habitacion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new HabitacionType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->container->get('lrotherfield.notify')->add("editar", array("message" => "Datos actualizados con exito."));
            // return $this->redirect($this->generateUrl('admin_habitacion_edit', array('id' => $id)));
            return $this->redirect($this->generateUrl('admin_habitacion'));
        }

        return $this->render('BackendBundle:Habitacion:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function deleteAction($id, $token)
    {
      $em = $this->getDoctrine()->getManager();
      $entity = $em->getRepository('BackendBundle:Habitacion')->find($id);
      if (!$entity) {
        throw $this->createNotFoundException('Unable to find habitacion entity.');
      }
      $csrf  = $this->container->get('form.csrf_provider');

      if ($csrf->isCsrfTokenValid($entity->getCsrfIntention('delete'), $token)) {
        $em->remove($entity);
        $em->flush();
        $this->container->get('lrotherfield.notify')->add("delete", array("message" => "Eliminado con exito."));
      } else {
            throw $this->createNotFoundException('Token es incorrecto.');
      }

      return $this->redirect($this->generateUrl('admin_habitacion'));

    }

    // /**
    //  * Deletes a Habitacion entity.
    //  *
    //  */
    // public function deleteAction(Request $request, $id)
    // {
    //     $form = $this->createDeleteForm($id);
    //     $form->bind($request);

    //     if ($form->isValid()) {
    //         $em = $this->getDoctrine()->getManager();
    //         $entity = $em->getRepository('BackendBundle:Habitacion')->find($id);

    //         if (!$entity) {
    //             throw $this->createNotFoundException('Unable to find Habitacion entity.');
    //         }

    //         $em->remove($entity);
    //         $em->flush();
    //     }

    //     return $this->redirect($this->generateUrl('admin_habitacion'));
    // }

    /**
     * Creates a form to delete a Habitacion entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
