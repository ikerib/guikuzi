<?php

namespace Gitek\Guikuzi\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Gitek\Guikuzi\BackendBundle\Entity\Residente;
use Gitek\Guikuzi\BackendBundle\Form\ResidenteType;

/**
 * Residente controller.
 *
 */
class ResidenteController extends Controller
{
    /**
     * Lists all Residente entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BackendBundle:Residente')->findAll();

        return $this->render('BackendBundle:Residente:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new Residente entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Residente();
        $form = $this->createForm(new ResidenteType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->container->get('lrotherfield.notify')->add("nuevo", array("message" => "Datos creados con exito."));
            return $this->redirect($this->generateUrl('admin_residente'));
            // return $this->redirect($this->generateUrl('admin_residente_show', array('id' => $entity->getId())));
        }

        return $this->render('BackendBundle:Residente:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Residente entity.
     *
     */
    public function newAction()
    {
        $entity = new Residente();
        $form   = $this->createForm(new ResidenteType(), $entity);

        return $this->render('BackendBundle:Residente:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Residente entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Residente')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Residente entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BackendBundle:Residente:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Residente entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Residente')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Residente entity.');
        }

        $editForm = $this->createForm(new ResidenteType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BackendBundle:Residente:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Residente entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Residente')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Residente entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new ResidenteType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->container->get('lrotherfield.notify')->add("editar", array("message" => "Datos actualizados con exito."));
            return $this->redirect($this->generateUrl('admin_residente'));
            // return $this->redirect($this->generateUrl('admin_residente_edit', array('id' => $id)));
        }

        return $this->render('BackendBundle:Residente:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }


    public function deleteAction($id, $token)
    {
      $em = $this->getDoctrine()->getManager();
      $entity = $em->getRepository('BackendBundle:Residente')->find($id);
      if (!$entity) {
        throw $this->createNotFoundException('Unable to find Residente entity.');
      }
      $csrf  = $this->container->get('form.csrf_provider');

      if ($csrf->isCsrfTokenValid($entity->getCsrfIntention('delete'), $token)) {
        $em->remove($entity);
        $em->flush();
        $this->container->get('lrotherfield.notify')->add("delete", array("message" => "Eliminado con exito."));
      } else {
            throw $this->createNotFoundException('Token es incorrecto.');
      }

      return $this->redirect($this->generateUrl('admin_residente'));

    }

    // /**
    //  * Deletes a Residente entity.
    //  *
    //  */
    // public function deleteAction(Request $request, $id)
    // {
    //     $form = $this->createDeleteForm($id);
    //     $form->bind($request);

    //     if ($form->isValid()) {
    //         $em = $this->getDoctrine()->getManager();
    //         $entity = $em->getRepository('BackendBundle:Residente')->find($id);

    //         if (!$entity) {
    //             throw $this->createNotFoundException('Unable to find Residente entity.');
    //         }

    //         $em->remove($entity);
    //         $em->flush();
    //     }

    //     return $this->redirect($this->generateUrl('admin_residente'));
    // }

    /**
     * Creates a form to delete a Residente entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
