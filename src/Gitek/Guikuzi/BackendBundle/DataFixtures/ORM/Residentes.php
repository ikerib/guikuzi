<?php

namespace Gitek\Guikuzi\BackenBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Gitek\Guikuzi\BackendBundle\Entity\Residente;


class Residentes extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder()
    {
        return 3;
    }

    public function load(ObjectManager $manager)
    {
        for ($i=0; $i < 10; $i++) {
            $hab = new Residente();
            $hab->setNombre('Residente' . $i);
            $hab->setApellido('Apellido' . $i);
            $hab->setCodigo('cod' . $i);
            $hab->setCodigodipu('codipu' . $i);
            $hab->setCodigonombre('codnombre' . $i);
            $manager->persist($hab);
        }

        $manager->flush();
    }
}
