<?php

namespace Gitek\Guikuzi\BackenBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Gitek\Guikuzi\BackendBundle\Entity\Jaula;


class Jaulas extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder()
    {
        return 4;
    }

    public function load(ObjectManager $manager)
    {
        for ($i=0; $i < 3; $i++) {
            $foo = new Jaula();
            $foo->setNombre('Jaula-' . $i);
            $foo->setComprobar(true);
            $manager->persist($foo);
        }

        $manager->flush();
    }
}
