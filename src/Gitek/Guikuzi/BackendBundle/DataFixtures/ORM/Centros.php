<?php

namespace Gitek\Guikuzi\BackenBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Gitek\Guikuzi\BackendBundle\Entity\Centro;


class Centros extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder()
    {
        return 2;
    }

    public function load(ObjectManager $manager)
    {
        $centro = new Centro();
        $centro->setNombre('Eibar');
        $manager->persist($centro);
        $manager->flush();
    }
}
