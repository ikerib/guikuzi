<?php

namespace Gitek\Guikuzi\BackenBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Gitek\Guikuzi\BackendBundle\Entity\Habitacion;


class Habitaciones extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder()
    {
        return 1;
    }

    public function load(ObjectManager $manager)
    {
        for ($i=0; $i < 10; $i++) {
            $hab = new Habitacion();
            $hab->setNombre('Habitación-' . $i);
            $hab->setCodigo('codhab' . $i);
            $manager->persist($hab);
        }

        $manager->flush();
    }
}
