<?php

namespace Gitek\Guikuzi\BackenBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Gitek\Guikuzi\BackendBundle\Entity\Jauladet;


class Jauladetalles extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder()
    {
        return 5;
    }

    public function load(ObjectManager $manager)
    {
        $jaula1 = $manager->getRepository('BackendBundle:Jaula')->findOneBy(array('nombre' => 'Jaula-0'));
        $jd = new Jauladet();
        $jd->setNombre('Detalle1');
        $jd->setJaula($jaula1);
        $manager->persist($jd);
        $jd = new Jauladet();
        $jd->setNombre('Detalle2');
        $jd->setJaula($jaula1);
        $manager->persist($jd);
        $jd = new Jauladet();
        $jd->setNombre('Detalle3');
        $jd->setJaula($jaula1);
        $manager->persist($jd);

        $jaula1 = $manager->getRepository('BackendBundle:Jaula')->findOneBy(array('nombre' => 'Jaula-1'));
        $jd = new Jauladet();
        $jd->setNombre('Detalle4');
        $jd->setJaula($jaula1);
        $manager->persist($jd);
        $jd = new Jauladet();
        $jd->setNombre('Detalle5');
        $jd->setJaula($jaula1);
        $manager->persist($jd);
        $jd = new Jauladet();
        $jd->setNombre('Detalle6');
        $jd->setJaula($jaula1);
        $manager->persist($jd);

        $jaula1 = $manager->getRepository('BackendBundle:Jaula')->findOneBy(array('nombre' => 'Jaula-2'));
        $jd = new Jauladet();
        $jd->setNombre('Detalle7');
        $jd->setJaula($jaula1);
        $manager->persist($jd);

        $manager->flush();
    }
}
