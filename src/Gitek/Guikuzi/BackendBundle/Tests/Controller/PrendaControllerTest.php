<?php

namespace Gitek\Guikuzi\BackendBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PrendaControllerTest extends WebTestCase
{
    public function testCompleteScenario()
    {
        // Create a new client to browse the application
        $client = static::createClient(array(), array(
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW'   => 'gitek',
        ));
        $client->enableProfiler();

        // Create a new entry in the database
        $crawler = $client->request('GET', '/admin/residente/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET /admin/residente/");

        // Miramos si tienen la plantilla 'Gitek'
        $this->assertEquals(1, $crawler->filter('.ultabs')->count() > 0, "(Index) Miramos si tenemos la plantilla de Gitek");
        $this->assertEquals(1, $crawler->filter('.menuaborde')->count() > 0, "(Index) Miramos si tenemos la plantilla de Gitek - Sidebar");
        $this->assertEquals(1, $crawler->filter('#niretabla')->count() > 0, "(Index) Miramos si tenemos la plantilla de Gitek - Tabla con contenido");

        /**************************************
         *** NUEVO
         **************************************/
        $crawler = $client->click($crawler->selectLink('Nuevo')->link());
        // (Nuevo) Miramos si tienen la plantilla 'Gitek'
        $this->assertEquals(1, $crawler->filter('.ultabs')->count() > 0, "(New) Miramos si tenemos la plantilla de Gitek");

        // Rellenamos el formulario
        $form = $crawler->selectButton('Create')->form(array(
            'gitek_guikuzi_backendbundle_residentetype[nombre]' => 'TestIzena',
            'gitek_guikuzi_backendbundle_residentetype[apellido]' => 'TestAbizena',
            'gitek_guikuzi_backendbundle_residentetype[codigo]' => 'TestKodea'
        ));

        $client->submit($form);
        // Si va a Show miramos si existe
        // $this->assertGreaterThan(0, $crawler->filter('td:contains("TestIzena")')->count(), 'Missing element td:contains("TestIzena")');

        // Miramos si redirige
        $this->assertTrue($client->getResponse()->isRedirect('/admin/residente/'), "No redirige a Residente(index)");
        $crawler = $client->followRedirect();


        /**************************************
         *** EDITAR
         **************************************/
        //Tiene que existir el botón "Editar"
        $crawler = $client->request('GET', '/admin/residente/');
        //Miramos si tiene theme "Gitek"
        $link = $crawler->selectLink('Editar')->link();
        $crawler = $client->click($link);

        $this->assertEquals(1, $crawler->filter('.ultabs')->count() > 0, "(Index) Miramos si tenemos la plantilla de Gitek");
        $this->assertEquals(1, $crawler->filter('.menuaborde')->count() > 0, "(Index) Miramos si tenemos la plantilla de Gitek - Sidebar");
        $form = $crawler->selectButton('Editar')->form(array(
            'gitek_guikuzi_backendbundle_residentetype[nombre]' => 'Foo',
        ));
        $client->submit($form);
        $this->assertTrue($client->getResponse()->isRedirect('/admin/residente/'), "No redirige a Residente(index)");
        $crawler = $client->followRedirect();

        /**************************************
         *** ELIMINAR
         **************************************/
        $crawler = $client->click($crawler->selectLink('Eliminar')->link());
        // Miramos si redirige
        $this->assertTrue($client->getResponse()->isRedirect('/admin/residente/'), "No redirige a Residente(index)(Eliminar)");
        $crawler = $client->followRedirect();


        /*
        * Miramos si existen las notificaciones
        */
        //$this->assertElementPresent('.humane', "(Notifikazioak) Notifikaziorik ez dago.");

    }
}