<?php

namespace Gitek\Guikuzi\BackendBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CentroControllerTest extends WebTestCase
{

    public function testCompleteScenario()
    {
        // Create a new client to browse the application
        $client = static::createClient(array(), array(
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW'   => 'gitek',
        ));

        // Create a new entry in the database
        $crawler = $client->request('GET', '/admin/centro/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET /admin/centro/");
        //         print_r(" ¿?¿?¿?¿?¿¿?¿?¿?¿?¿? ");
        // print_r( $crawler->selectLink('Nuevo')->link());
        //$crawler = $client->click($crawler->selectLink('Nuevo')->link());

        // Fill in the form and submit it
        //$form = $crawler->selectButton('Nuevo')->form(array(
         //   'gitek_guikuzi_backendbundle_centrotype[nombre]' => 'Test',
            // ... other fields to fill
        //));

        // $client->submit($form);
        // $crawler = $client->followRedirect();

        // // Check data in the show view
        // $this->assertGreaterThan(0, $crawler->filter('td:contains("Test")')->count(), 'Missing element td:contains("Test")');

        // // Edit the entity
        // $crawler = $client->click($crawler->selectLink('Edit')->link());

        // $form = $crawler->selectButton('Edit')->form(array(
        //     'gitek_guikuzi_backendbundle_centrotype[field_name]'  => 'Foo',
        //     // ... other fields to fill
        // ));

        // $client->submit($form);
        // $crawler = $client->followRedirect();

        // // Check the element contains an attribute with value equals "Foo"
        // $this->assertGreaterThan(0, $crawler->filter('[value="Foo"]')->count(), 'Missing element [value="Foo"]');

        // // Delete the entity
        // $client->submit($crawler->selectButton('Delete')->form());
        // $crawler = $client->followRedirect();

        // // Check the entity has been delete on the list
        // $this->assertNotRegExp('/Foo/', $client->getResponse()->getContent());
    }


}