<?php

namespace Gitek\Guikuzi\BackendBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HabitacionControllerTest extends WebTestCase
{

    public function testCompleteScenario()
    {
        // Create a new client to browse the application
        $client = static::createClient();

        // Create a new entry in the database
        $crawler = $client->request('GET', '/admin/habitacion/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET /admin/habitacion/");

        // Miramos si tienen la plantilla 'Gitek'
        $this->assertEquals(1, $crawler->filter('.ultabs')->count() > 0, "(Index) Miramos si tenemos la plantilla de Gitek");
        $this->assertEquals(1, $crawler->filter('.menuaborde')->count() > 0, "(Index) Miramos si tenemos la plantilla de Gitek - Sidebar");
        $this->assertEquals(1, $crawler->filter('#niretabla')->count() > 0, "(Index) Miramos si tenemos la plantilla de Gitek - Tabla con contenido");


        $crawler = $client->click($crawler->selectLink('Nuevo')->link());

        // Miramos si tienen la plantilla 'Gitek'
        $this->assertEquals(1, $crawler->filter('.ultabs')->count() > 0, "(New) Miramos si tenemos la plantilla de Gitek");

        // Fill in the form and submit it
        $form = $crawler->selectButton('Create')->form(array(
            'gitek_guikuzi_backendbundle_habitaciontype[nombre]' => 'Testa'
        ));

        $client->submit($form);
        $crawler = $client->followRedirect();

        // Check data in the show view
        $this->assertGreaterThan(0, $crawler->filter('td:contains("Testa")')->count(), 'Missing element td:contains("Testa")');

        // Edit the entity
        $crawler = $client->click($crawler->selectLink('Editar')->link());

        $form = $crawler->selectButton('Editar')->form(array(
            'gitek_guikuzi_backendbundle_habitaciontype[nombre]' => 'Foo',
        ));

        $client->submit($form);
        $crawler = $client->followRedirect();

        // Delete the entity
        $crawler = $client->click($crawler->selectLink('Eliminar')->link());
        $crawler = $client->followRedirect();

        // Check the entity has been delete on the list
        // $this->assertNotRegExp('/Testa/', $client->getResponse()->getContent());


    }


}