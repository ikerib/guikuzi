<?php

namespace Gitek\Guikuzi\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Residente
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Gitek\Guikuzi\BackendBundle\Entity\ResidenteRepository")
 */
class Residente
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=false)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido", type="string", length=255, nullable=true)
     */
    private $apellido;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=100, nullable=true)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="codigodipu", type="string", length=100, nullable=true)
     */
    private $codigodipu;

    /**
     * @var string
     *
     * @ORM\Column(name="codigonombre", type="string", length=100, nullable=true)
     */
    private $codigonombre;

    /**
     * @var \DateTime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var \DateTime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\OneToMany(targetEntity="Config", mappedBy="residente", cascade={"remove"})
     */
    protected $config;

    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
        $this->codigodipu = 'D';
        $this->codigonombre = 'R';
    }

    public function __toString()
    {
        return $this->getNombre();
    }

    public function getCsrfIntention($intention)
    {
      return sha1(get_class($this).$intention.$this->id);
    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Residente
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido
     *
     * @param string $apellido
     * @return Residente
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;

        return $this;
    }

    /**
     * Get apellido
     *
     * @return string
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     * @return Residente
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set codigodipu
     *
     * @param string $codigodipu
     * @return Residente
     */
    public function setCodigodipu($codigodipu)
    {
        $this->codigodipu = $codigodipu;

        return $this;
    }

    /**
     * Get codigodipu
     *
     * @return string
     */
    public function getCodigodipu()
    {
        return $this->codigodipu;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Residente
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Residente
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Add config
     *
     * @param \Gitek\Guikuzi\BackendBundle\Entity\Config $config
     * @return Residente
     */
    public function addConfig(\Gitek\Guikuzi\BackendBundle\Entity\Config $config)
    {
        $this->config[] = $config;

        return $this;
    }

    /**
     * Remove config
     *
     * @param \Gitek\Guikuzi\BackendBundle\Entity\Config $config
     */
    public function removeConfig(\Gitek\Guikuzi\BackendBundle\Entity\Config $config)
    {
        $this->config->removeElement($config);
    }

    /**
     * Get config
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Set codigonombre
     *
     * @param string $codigonombre
     * @return Residente
     */
    public function setCodigonombre($codigonombre)
    {
        $this->codigonombre = $codigonombre;

        return $this;
    }

    /**
     * Get codigonombre
     *
     * @return string
     */
    public function getCodigonombre()
    {
        return $this->codigonombre;
    }
}