<?php
/**
 * User: Iker Ibarguren
 * Date: 09/04/14
 * Time: 09:56
 */

namespace Gitek\Guikuzi\BackendBundle\Entity;

use Doctrine\ORM\EntityRepository;

class RegistroRepository extends EntityRepository
{

    public function getdatuak($desde=null, $hasta=null) {

        $em = $this->getEntityManager();

        $consulta = $em->createQuery('SELECT count (r.resultado) as Zenbat, r.descripcion, r.resultado
                                    FROM BackendBundle:Registro r
                                    WHERE r.created_at BETWEEN :desde and :hasta
                                    GROUP BY r.resultado
                                    ORDER BY r.resultado DESC
                                  ');

        if ($desde == null) {
            $consulta->setParameter('desde', new \DateTime('1970-01-01'));
        } else {
//            $consulta->setParameter('desde', $desde,\Doctrine\DBAL\Types\Type::DATETIME);
            $consulta->setParameter('desde', new \DateTime($desde));
        }

        if ($desde == null) {
            $consulta->setParameter('hasta', new \DateTime());
        } else {
//            $consulta->setParameter('hasta', $hasta, \Doctrine\DBAL\Types\Type::DATETIME);
            $consulta->setParameter('hasta', new \DateTime($hasta));
        }

        $misql = $consulta->getSql();

        return $consulta->getResult();

    }

    public function getdatuakinfo($desde=null, $hasta=null) {

        $em = $this->getEntityManager();

        $consulta = $em->createQuery('SELECT count (r.resultado) as Zenbat, r.descripcion, r.resultado
                                    FROM BackendBundle:Registro r
                                    WHERE r.created_at BETWEEN :desde and :hasta
                                    GROUP BY r.resultado
                                    ORDER BY r.resultado DESC
                                  ');

        if ($desde == null) {
            $consulta->setParameter('desde', new \DateTime('1970-01-01'));
        } else {
//            $consulta->setParameter('desde', $desde,\Doctrine\DBAL\Types\Type::DATETIME);
            $consulta->setParameter('desde', new \DateTime($desde));
        }

        if ($desde == null) {
            $consulta->setParameter('hasta', new \DateTime());
        } else {
//            $consulta->setParameter('hasta', $hasta, \Doctrine\DBAL\Types\Type::DATETIME);
            $consulta->setParameter('hasta', new \DateTime($hasta));
        }

        $misql = $consulta->getSql();

        return $consulta->getResult();

    }

}

