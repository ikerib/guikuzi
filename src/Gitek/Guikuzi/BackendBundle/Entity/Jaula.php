<?php

namespace Gitek\Guikuzi\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Jaula
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Gitek\Guikuzi\BackendBundle\Entity\JaulaRepository")
 */
class Jaula
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var boolean
     *
     * @ORM\Column(name="comprobar", type="boolean", length=1)
     */
    private $comprobar;

    /**
     * @ORM\OneToMany(targetEntity="Jauladet", mappedBy="jaula", cascade={"remove"})
     */
    protected $jauladets;

    /**
     * @ORM\OneToMany(targetEntity="Config", mappedBy="jaula", cascade={"remove"})
     */
    protected $config;

    /**
     * @var \DateTime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var \DateTime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
        $this->jauladets = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getNombre();
    }

    public function getCsrfIntention($intention)
    {
      return sha1(get_class($this).$intention.$this->id);
    }



    public function setJauladets(ArrayCollection $jauladets)
    {
        foreach ($jauladets as $jauladet) {
            $jauladet->addJaula($this);
        }

        $this->jauladets = $jauladets;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Jaula
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Jaula
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Jaula
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Add jauladets
     *
     * @param \Gitek\Guikuzi\BackendBundle\Entity\Jauladet $jauladets
     * @return Jaula
     */
    public function addJauladet(\Gitek\Guikuzi\BackendBundle\Entity\Jauladet $jauladets)
    {
        $this->jauladets[] = $jauladets;
        $jauladets->setJaula($this);
        return $this;
    }

    /**
     * Remove jauladets
     *
     * @param \Gitek\Guikuzi\BackendBundle\Entity\Jauladet $jauladets
     */
    public function removeJauladet(\Gitek\Guikuzi\BackendBundle\Entity\Jauladet $jauladets)
    {
        $this->jauladets->removeElement($jauladets);
    }

    /**
     * Get jauladets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJauladets()
    {
        return $this->jauladets;
    }

    /**
     * Set comprobar
     *
     * @param boolean $comprobar
     * @return Jaula
     */
    public function setComprobar($comprobar)
    {
        $this->comprobar = $comprobar;
    
        return $this;
    }

    /**
     * Get comprobar
     *
     * @return boolean 
     */
    public function getComprobar()
    {
        return $this->comprobar;
    }

    /**
     * Add config
     *
     * @param \Gitek\Guikuzi\BackendBundle\Entity\Config $config
     * @return Jaula
     */
    public function addConfig(\Gitek\Guikuzi\BackendBundle\Entity\Config $config)
    {
        $this->config[] = $config;
    
        return $this;
    }

    /**
     * Remove config
     *
     * @param \Gitek\Guikuzi\BackendBundle\Entity\Config $config
     */
    public function removeConfig(\Gitek\Guikuzi\BackendBundle\Entity\Config $config)
    {
        $this->config->removeElement($config);
    }

    /**
     * Get config
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getConfig()
    {
        return $this->config;
    }
}