<?php

namespace Gitek\Guikuzi\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Parametros
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Gitek\Guikuzi\BackendBundle\Entity\ParametrosRepository")
 */
class Parametros
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="comprobar", type="boolean", nullable=true)
	 */
	private $comprobar;


	/**
	 * @ORM\ManyToOne(targetEntity="Centro")
	 * @ORM\JoinColumn(name="centro_id", referencedColumnName="id", nullable=true)
	 */
	protected $centro;

  /**
   * @var \DateTime $created_at
   *
   * @ORM\Column(name="created_at", type="datetime", nullable=true)
   */
  private $created_at;

  /**
   * @var \DateTime $updated_at
   *
   * @ORM\Column(name="updated_at", type="datetime", nullable=true)
   */
  private $updated_at;


  public function __construct()
  {
    $this->created_at = new \DateTime();
    $this->updated_at = new \DateTime();
  }

  public function __toString()
  {
    return $this->getNombre();
  }

  public function getCsrfIntention($intention)
  {
    return sha1(get_class($this).$intention.$this->id);
  }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comprobar
     *
     * @param boolean $comprobar
     * @return Parametros
     */
    public function setComprobar($comprobar)
    {
        $this->comprobar = $comprobar;

        return $this;
    }

    /**
     * Get comprobar
     *
     * @return boolean
     */
    public function getComprobar()
    {
        return $this->comprobar;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Parametros
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Parametros
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set centro
     *
     * @param \Gitek\Guikuzi\BackendBundle\Entity\Centro $centro
     * @return Parametros
     */
    public function setCentro(\Gitek\Guikuzi\BackendBundle\Entity\Centro $centro = null)
    {
        $this->centro = $centro;

        return $this;
    }

    /**
     * Get centro
     *
     * @return \Gitek\Guikuzi\BackendBundle\Entity\Centro
     */
    public function getCentro()
    {
        return $this->centro;
    }
}