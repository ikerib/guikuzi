<?php

namespace Gitek\Guikuzi\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Config
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Gitek\Guikuzi\BackendBundle\Entity\ConfigRepository")
 */
class Config
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=true)
     */
    private $nombre;

    /**
     * @ORM\ManyToOne(targetEntity="Habitacion", inversedBy="config")
     * @ORM\JoinColumn(name="habitacion_id", referencedColumnName="id", nullable=true)
     */
    protected $habitacion;

    /**
     * @ORM\ManyToOne(targetEntity="Jaula", inversedBy="config")
     * @ORM\JoinColumn(name="jaula_id", referencedColumnName="id", nullable=true)
     */
    protected $jaula;

    /**
     * @ORM\ManyToOne(targetEntity="Jauladet", inversedBy="config")
     * @ORM\JoinColumn(name="jauladet_id", referencedColumnName="id", nullable=true)
     */
    protected $jauladet;

    /**
     * @ORM\ManyToOne(targetEntity="Residente", inversedBy="config")
     * @ORM\JoinColumn(name="residente_id", referencedColumnName="id", nullable=true)
     */
    protected $residente;

    /**
     * @var \DateTime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var \DateTime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
    }

    public function __toString()
    {
        return $this->getNombre();
    }

    public function getCsrfIntention($intention)
    {
      return sha1(get_class($this).$intention.$this->id);
    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Config
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Config
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Config
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set habitacion
     *
     * @param \Gitek\Guikuzi\BackendBundle\Entity\Habitacion $habitacion
     * @return Config
     */
    public function setHabitacion(\Gitek\Guikuzi\BackendBundle\Entity\Habitacion $habitacion = null)
    {
        $this->habitacion = $habitacion;

        return $this;
    }

    /**
     * Get habitacion
     *
     * @return \Gitek\Guikuzi\BackendBundle\Entity\Habitacion
     */
    public function getHabitacion()
    {
        return $this->habitacion;
    }

    /**
     * Set jaula
     *
     * @param \Gitek\Guikuzi\BackendBundle\Entity\Jaula $jaula
     * @return Config
     */
    public function setJaula(\Gitek\Guikuzi\BackendBundle\Entity\Jaula $jaula = null)
    {
        $this->jaula = $jaula;

        return $this;
    }

    /**
     * Get jaula
     *
     * @return \Gitek\Guikuzi\BackendBundle\Entity\Jaula
     */
    public function getJaula()
    {
        return $this->jaula;
    }

    /**
     * Set residente
     *
     * @param \Gitek\Guikuzi\BackendBundle\Entity\Residente $residente
     * @return Config
     */
    public function setResidente(\Gitek\Guikuzi\BackendBundle\Entity\Residente $residente = null)
    {
        $this->residente = $residente;

        return $this;
    }

    /**
     * Get residente
     *
     * @return \Gitek\Guikuzi\BackendBundle\Entity\Residente
     */
    public function getResidente()
    {
        return $this->residente;
    }

    /**
     * Set jauladet
     *
     * @param \Gitek\Guikuzi\BackendBundle\Entity\Jauladet $jauladet
     * @return Config
     */
    public function setJauladet(\Gitek\Guikuzi\BackendBundle\Entity\Jauladet $jauladet = null)
    {
        $this->jauladet = $jauladet;

        return $this;
    }

    /**
     * Get jauladet
     *
     * @return \Gitek\Guikuzi\BackendBundle\Entity\Jauladet
     */
    public function getJauladet()
    {
        return $this->jauladet;
    }
}