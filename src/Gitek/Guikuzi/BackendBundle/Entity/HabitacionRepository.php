<?php

namespace Gitek\Guikuzi\BackendBundle\Entity;

use Doctrine\ORM\EntityRepository;

class HabitacionRepository extends EntityRepository{

    public function bilatugelak() {

        $em = $this->getEntityManager();

        $consulta = $em->createQuery('SELECT h
                                    FROM BackendBundle:Habitacion h
                                    LEFT JOIN h.config c
                                    WHERE c.habitacion is NULL
                                  ');

        return $consulta->getResult();

    }

}