<?php

namespace Gitek\Guikuzi\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Jauladet
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @Vich\Uploadable
 */
class Jauladet
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre; // 2014-05-29 se usará como código

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255, nullable=true)
     */
    private $descripcion;

    /**
     * @Assert\File(
     *     maxSize="10M",
     *     mimeTypes={"image/png", "image/jpeg", "image/pjpeg"}
     * )
     * @Vich\UploadableField(mapping="uploads", fileNameProperty="foto", nullable=true)
     *
     * @var File $image
     */
    protected $image;

    /**
     * @ORM\Column(type="string", length=255, name="foto", nullable=true)
     *
     * @var string $foto
     */
    protected $foto;

    /**
     * @ORM\ManyToOne(targetEntity="Jaula", inversedBy="jauladets")
     * @ORM\JoinColumn(name="jaula_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $jaula;

    /**
     * @ORM\OneToMany(targetEntity="Config", mappedBy="jauladet", cascade={"remove"})
     */
    protected $config;

    /**
     * @var \DateTime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var \DateTime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
        $this->codigo = 'J';
    }

    public function setImage($image)
    {
        $this->image = $image;
        if ($image instanceof UploadedFile) {
            $this->setUpdatedAt(new \DateTime());
        }
    }

    public function getImage()
    {
        return $this->image;
    }

    public function __toString()
    {
        return $this->getNombre();
    }

    public function getCsrfIntention($intention)
    {
        return sha1(get_class($this) . $intention . $this->id);
    }

    public function addJaula(Jaula $jaula)
    {
        if (!$this->jaula->contains($jaula)) {
            $this->jaula->add($jaula);
        }
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set foto
     *
     * @param string $foto
     * @return Jauladet
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;

        return $this;
    }

    /**
     * Get foto
     *
     * @return string
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Jauladet
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Jauladet
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set jaula
     *
     * @param \Gitek\Guikuzi\BackendBundle\Entity\Jaula $jaula
     * @return Jauladet
     */
    public function setJaula(\Gitek\Guikuzi\BackendBundle\Entity\Jaula $jaula = null)
    {
        $this->jaula = $jaula;

        return $this;
    }

    /**
     * Get jaula
     *
     * @return \Gitek\Guikuzi\BackendBundle\Entity\Jaula
     */
    public function getJaula()
    {
        return $this->jaula;
    }

    /**
     * Add config
     *
     * @param \Gitek\Guikuzi\BackendBundle\Entity\Config $config
     * @return Jauladet
     */
    public function addConfig(\Gitek\Guikuzi\BackendBundle\Entity\Config $config)
    {
        $this->config[] = $config;

        return $this;
    }

    /**
     * Remove config
     *
     * @param \Gitek\Guikuzi\BackendBundle\Entity\Config $config
     */
    public function removeConfig(\Gitek\Guikuzi\BackendBundle\Entity\Config $config)
    {
        $this->config->removeElement($config);
    }

    /**
     * Get config
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Jauladet
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Jauladet
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
}