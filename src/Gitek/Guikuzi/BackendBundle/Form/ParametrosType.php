<?php

namespace Gitek\Guikuzi\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ParametrosType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // ->add('comprobar')
            ->add('comprobar', null , array(
                    'attr'=> array(
                        'placeholder'=>'',
                        'class'=>'MYCLASSFOR_INPUTS'
                    ) ,
                    'label_attr' => array(
                        'class' => 'MYCLASSFOR_LABEL'
                    )
                ))
            // ->add('centro')
            ->add('centro', 'entity', array(
                'class' => 'BackendBundle:Centro',
                'query_builder' => function($repository) { return $repository->createQueryBuilder('p')->orderBy('p.id', 'ASC'); },
                'property' => 'nombre',
                'attr'=> array(
                        'placeholder'=>'',
                        'class'=>'MYCLASSFOR_INPUTS'
                    ) ,
                    'label_attr' => array(
                        'class' => 'MYCLASSFOR_LABEL'
                    )
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Gitek\Guikuzi\BackendBundle\Entity\Parametros'
        ));
    }

    public function getName()
    {
        return 'frmparametros';
    }
}
