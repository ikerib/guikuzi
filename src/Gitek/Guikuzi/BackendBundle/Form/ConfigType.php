<?php

namespace Gitek\Guikuzi\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ConfigType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('jaula')
            ->add('jauladet')
            ->add('residente')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Gitek\Guikuzi\BackendBundle\Entity\Config'
        ));
    }

    public function getName()
    {
        return 'gitek_guikuzi_backendbundle_configtype';
    }
}
