<?php

namespace Gitek\Guikuzi\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class JauladetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('jaula', null , array(
                'label' => 'Jaula: ',
                'required' => true,
                'attr'=> array(
                    'placeholder'=>'',
                    'class'=>'MYCLASSFOR_INPUTS'
                ) ,
                'label_attr' => array(
                    'class' => 'MYCLASSFOR_LABEL'
                )
            ))
            ->add('nombre', null , array(
                'label' => 'Código: ',
                'required' => true,
                'attr'=> array(
                    'placeholder'=>'',
                    'class'=>'MYCLASSFOR_INPUTS'
                ) ,
                'label_attr' => array(
                    'class' => 'MYCLASSFOR_LABEL'
                )
            ))
            ->add('descripcion', null , array(
                'label' => 'Descripción: ',
                'required' => true,
                'attr'=> array(
                    'placeholder'=>'',
                    'class'=>'MYCLASSFOR_INPUTS'
                ) ,
                'label_attr' => array(
                    'class' => 'MYCLASSFOR_LABEL'
                )
            ))
            ->add('image', 'file', array(
                'attr'=> array(
                    'placeholder'=>'',
                    'class'=>'MYCLASSFOR_INPUTS'
                ) ,
                'label_attr' => array(
                    'class' => 'MYCLASSFOR_LABEL'
                ),
                'data_class' => 'Symfony\Component\HttpFoundation\File\File',
                'required' => false,
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Gitek\Guikuzi\BackendBundle\Entity\Jauladet'
        ));
    }

    public function getName()
    {
        return 'jauladet';
    }
}
