<?php

namespace Gitek\Guikuzi\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ResidenteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', null , array(
                    'required' => true,
                    'attr'=> array(
                        'placeholder'=>'',
                        'class'=>'MYCLASSFOR_INPUTS'
                    ) ,
                    'label_attr' => array(
                        'class' => 'MYCLASSFOR_LABEL'
                    )
                ))
            ->add('apellido', null , array(
                'required' => true,
                    'attr'=> array(
                        'placeholder'=>'',
                        'class'=>'MYCLASSFOR_INPUTS'
                    ) ,
                    'label_attr' => array(
                        'class' => 'MYCLASSFOR_LABEL'
                    )
                ))
            ->add('codigo', null , array(
                    'label' => 'Código Usuario:',
                    'attr'=> array(
                        'placeholder'=>'',
                        'class'=>'MYCLASSFOR_INPUTS'
                    ) ,
                    'label_attr' => array(
                        'class' => 'MYCLASSFOR_LABEL'
                    )
                ))
            ->add('codigodipu', null , array(
                    'label' => 'Código Diputación:',
                    'attr'=> array(
                        'placeholder'=>'',
                        'class'=>'MYCLASSFOR_INPUTS'
                    ) ,
                    'label_attr' => array(
                        'class' => 'MYCLASSFOR_LABEL'
                    )
                ))
            ->add('codigonombre', null , array(
                    'label' => 'Código Residente:',
                    'attr'=> array(
                        'placeholder'=>'',
                        'class'=>'MYCLASSFOR_INPUTS'
                    ) ,
                    'label_attr' => array(
                        'class' => 'MYCLASSFOR_LABEL'
                    )
                ));
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Gitek\Guikuzi\BackendBundle\Entity\Residente'
        ));
    }

    public function getName()
    {
        return 'gitek_guikuzi_backendbundle_residentetype';
    }
}
