<?php

namespace Gitek\Guikuzi\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class JaulaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', null , array(
                    'required' => true,
                    'attr'=> array(
                        'placeholder'=>'',
                        'class'=>'MYCLASSFOR_INPUTS'
                    ) ,
                    'label_attr' => array(
                        'class' => 'MYCLASSFOR_LABEL'
                    )
                ))
            ->add('comprobar')
            ->add('jauladets', 'collection', array(
                'attr'=> array(
                    'placeholder'=>'',
                    'class'=>'MYCLASSFOR_INPUTS'
                ) ,
                'label_attr' => array(
                    'class' => 'MYCLASSFOR_LABEL'
                ),
                'type'         => new JauladetType(),
                'allow_add'    => true,
                'allow_delete' => true,
                'by_reference' => false,
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Gitek\Guikuzi\BackendBundle\Entity\Jaula'
        ));
    }

    public function getName()
    {
        return 'jaula';
    }
}
