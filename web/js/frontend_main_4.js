angular.module('Guikuzi', ['ngResource'])

    .directive('onEnter', function() {
        return function(scope, element, attrs) {
            element.bind("keydown keypress", function(event) {
                if(event.which === 13) {
                    scope.$apply(function(){
                        scope.$eval(attrs.onEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    })


.controller('PrendaCtrl', function( $scope, $resource)
{

  $scope.BuscaPrenda = $resource('/bilaketa/:codbar', {codbar:'@id'});
  $scope.placeholder = "Leer código de barras de Residente";
  $scope.bilaketa = function()
  {
    $scope.prenda = $scope.BuscaPrenda.get(
        { codbar: $scope.codbar },
        function(erantzuna)
        {

            if ( erantzuna.mp3 !== "" ) {
                var niremp3 ="/media/" + erantzuna.mp3;

                var audio = document.getElementById("myplayer");
                var source=document.getElementById('source');
                source.src=niremp3;

                audio.load(); //call this to just preload the audio without playing
                audio.play(); //call this to play the song right away

            }



            $scope.placeholder = "Leer código de barras de Hueco";
            if ( erantzuna.completed === "1" ) {
                $scope.placeholder = "Leer código de barras de Residente";
            }
        }
    );
    $scope.codbar = "";
  };

});


