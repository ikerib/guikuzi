angular.module('Guikuzi', ['ngAudio','ngResource']).directive('onEnter', function() {
        return function(scope, element, attrs) {
            element.bind("keydown keypress", function(event) {
                if(event.which === 13) {
                    scope.$apply(function(){
                        scope.$eval(attrs.onEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    });


function PrendaCtrl( $scope, $resource, ngAudio)
{
    $scope.audios = [
        ngAudio.load('media/correcto.mp3'),
        ngAudio.load('media/incorrecto.mp3'),
        ngAudio.load('media/noencontrado.mp3'),
    ];
    $scope.sound = ngAudio.load('media/correcto.mp3');
  $scope.BuscaPrenda = $resource('/bilaketa/:codbar', {codbar:'@id'});
  $scope.placeholder = "Leer código de barras de Residente";
  $scope.bilaketa = function()
  {
    $scope.prenda = $scope.BuscaPrenda.get(
        { codbar: $scope.codbar },
        function(erantzuna)
        {
            $scope.placeholder = "Leer código de barras de Hueco";
            if ( erantzuna.completed === "1" ) {
                $scope.placeholder = "Leer código de barras de Residente";
            }
        }
    );
    $scope.codbar = "";
  };

}


