// Generated by CoffeeScript 1.6.1
(function() {

  $(document).ready(function() {
    $(".btnconfigmenu").click(function() {
      var miurl;
      miurl = $(this).data('menu');
      console.log(".div" + miurl);
      return $(".div" + miurl).animate({
        "left": -($(id).position().left)
      }, 600);
    });
    $('#seljau').change(function() {
      var jaulaid, jaulatext, that, url;
      that = this;
      jaulaid = this.value;
      if (!jaulaid) {
        return alert("Selecciona Jaula");
      }
      url = Routing.generate("api_irudiak", {
        jaulaid: this.value
      });
      $.getJSON(url, function(json) {
        $('#gallery ul').empty();
        return json.forEach(function(dato) {
          return $('#gallery').append("<li><a class='fancybox-thumb' rel='fancybox-thumb' href='/images/uploads/" + dato.foto + "'  data-jauladetid=" + dato.id + "><img data-jauladetid=" + dato.id + " src='/images/uploads/" + dato.foto + "'></a></li>");
        });
      });
      $('#gitek_guikuzi_backendbundle_configtype_jaula option:selected').prop("selected", false);
      $('#gitek_guikuzi_backendbundle_configtype_jaula option').filter(function() {
        return this.value === jaulaid;
      }).attr("selected", true);
      $('#gitek_guikuzi_backendbundle_configtype_jaula').val(jaulaid);
      jaulatext = $("#gitek_guikuzi_backendbundle_configtype_jaula option:selected").text();
      $('.tdjaula').append("<span>Jaula: " + jaulatext + "</span>");
      return $(".fancybox-thumb").fancybox({
        width: 600,
        height: 600,
        prevEffect: "none",
        nextEffect: "none",
        closeClick: true,
        afterClose: function() {
          return console.log("itxi da!");
        },
        helpers: {
          overlay: {
            opacity: 0.6,
            css: {
              "background-color": "#cccccc"
            }
          },
          title: {
            type: "inside"
          },
          thumbs: {
            position: "top",
            width: 50,
            height: 50
          }
        }
      });
    });
    $("#btnseleccionar").click(function(e) {
      e.preventDefault();
      return $(".fancybox-thumb:eq(0)").click();
    });
    $("#selhab").change(function() {
      var hab, habtext;
      hab = this.value;
      if (!hab) {
        return alert("Selecciona habitacion!");
      }
      $("#gitek_guikuzi_backendbundle_configtype_habitacion option:selected").prop("selected", false);
      $("#gitek_guikuzi_backendbundle_configtype_habitacion option").filter(function() {
        return this.value === hab;
      }).attr("selected", true);
      $("#gitek_guikuzi_backendbundle_configtype_habitacion").val(hab);
      habtext = $("#gitek_guikuzi_backendbundle_configtype_habitacion option:selected").text();
      return $('.tdcodbar').append("<span>Habitacion: " + habtext + "</span>");
    });
    $("#selres").change(function() {
      var res, restext;
      res = this.value;
      if (!res) {
        return alert("Selecciona residente!");
      }
      $("#gitek_guikuzi_backendbundle_configtype_residente option:selected").prop("selected", false);
      $("#gitek_guikuzi_backendbundle_configtype_residente option").filter(function() {
        return this.value === res;
      }).attr("selected", true);
      $("#gitek_guikuzi_backendbundle_configtype_residente").val(res);
      restext = $("#gitek_guikuzi_backendbundle_configtype_residente option:selected").text();
      return $('.tdcodbar').append("<span>Residente: " + restext + "</span>");
    });
    $("#selres").change(function() {
      var hab;
      hab = this.value;
      $("#gitek_guikuzi_backendbundle_configtype_residente option:selected").prop("selected", false);
      $("#gitek_guikuzi_backendbundle_configtype_residente option").filter(function() {
        return this.value === hab;
      }).attr("selected", true);
      return $("#gitek_guikuzi_backendbundle_configtype_residente").val(hab);
    });
    $(document).on("click", ".imgjauladet", function() {
      var jauladetid, miclon;
      jauladetid = $(this).data('jauladetid');
      $('#gitek_guikuzi_backendbundle_configtype_jauladet option:selected').prop("selected", false);
      $('#gitek_guikuzi_backendbundle_configtype_jauladet option').filter(function() {
        return this.value === jauladetid;
      }).attr("selected", true);
      $('#gitek_guikuzi_backendbundle_configtype_jauladet').val(jauladetid);
      $('.divjaula').slideUp("slow");
      console.log($(this).children());
      miclon = $(this).clone();
      return $('.tdjauladet').append(miclon);
    });
    return $(".btnno").click(function() {
      return $("form").submit();
    });
  });

}).call(this);
